-- Merging decision tree log ---
manifest
ADDED from AndroidManifest.xml:2:1
	package
		ADDED from AndroidManifest.xml:3:5
		INJECTED from AndroidManifest.xml:0:0
		INJECTED from AndroidManifest.xml:0:0
	android:versionName
		ADDED from AndroidManifest.xml:5:5
	android:versionCode
		ADDED from AndroidManifest.xml:4:5
		INJECTED from AndroidManifest.xml:0:0
		INJECTED from AndroidManifest.xml:0:0
	xmlns:android
		ADDED from AndroidManifest.xml:2:11
uses-sdk
ADDED from AndroidManifest.xml:7:5
MERGED from com.android.support:appcompat-v7:19.0.1:18:5
	android:targetSdkVersion
		ADDED from AndroidManifest.xml:9:9
		INJECTED from AndroidManifest.xml:0:0
		INJECTED from AndroidManifest.xml:0:0
	android:minSdkVersion
		ADDED from AndroidManifest.xml:8:9
		INJECTED from AndroidManifest.xml:0:0
		INJECTED from AndroidManifest.xml:0:0
uses-permission#android.permission.INTERNET
ADDED from AndroidManifest.xml:11:5
	android:name
		ADDED from AndroidManifest.xml:11:22
uses-permission#android.permission.ACCESS_NETWORK_STATE
ADDED from AndroidManifest.xml:12:5
	android:name
		ADDED from AndroidManifest.xml:12:22
uses-permission#android.permission.READ_PHONE_STATE
ADDED from AndroidManifest.xml:13:5
	android:name
		ADDED from AndroidManifest.xml:13:22
uses-permission#android.permission.READ_EXTERNAL_STORAGE
ADDED from AndroidManifest.xml:14:5
	android:name
		ADDED from AndroidManifest.xml:14:22
uses-permission#android.permission.ACCESS_SUPERUSER
ADDED from AndroidManifest.xml:15:5
	android:name
		ADDED from AndroidManifest.xml:15:22
uses-permission#android.permission.RECEIVE_BOOT_COMPLETED
ADDED from AndroidManifest.xml:16:5
	android:name
		ADDED from AndroidManifest.xml:16:22
uses-permission#android.permission.CAMERA
ADDED from AndroidManifest.xml:18:5
	android:name
		ADDED from AndroidManifest.xml:18:22
uses-feature#android.hardware.camera.autofocus
ADDED from AndroidManifest.xml:19:5
	android:name
		ADDED from AndroidManifest.xml:19:19
uses-feature#android.hardware.camera.front
ADDED from AndroidManifest.xml:20:5
	android:required
		ADDED from AndroidManifest.xml:20:64
	android:name
		ADDED from AndroidManifest.xml:20:19
uses-feature#android.hardware.camera
ADDED from AndroidManifest.xml:22:5
	android:required
		ADDED from AndroidManifest.xml:24:9
	android:name
		ADDED from AndroidManifest.xml:23:9
uses-permission#android.permission.WRITE_EXTERNAL_STORAGE
ADDED from AndroidManifest.xml:26:5
	android:name
		ADDED from AndroidManifest.xml:26:22
application
ADDED from AndroidManifest.xml:28:5
MERGED from com.android.support:appcompat-v7:19.0.1:19:5
	android:label
		ADDED from AndroidManifest.xml:31:9
	android:allowBackup
		ADDED from AndroidManifest.xml:29:9
	android:icon
		ADDED from AndroidManifest.xml:30:9
	android:theme
		ADDED from AndroidManifest.xml:32:9
service#com.peakbw.loanapp.services.DownloadService
ADDED from AndroidManifest.xml:33:9
	android:exported
		ADDED from AndroidManifest.xml:35:13
	android:name
		ADDED from AndroidManifest.xml:34:13
activity#com.peakbw.loanapp.activities.MainActivity
ADDED from AndroidManifest.xml:37:9
	android:screenOrientation
		ADDED from AndroidManifest.xml:40:13
	android:label
		ADDED from AndroidManifest.xml:39:13
	android:theme
		ADDED from AndroidManifest.xml:41:13
	android:name
		ADDED from AndroidManifest.xml:38:13
intent-filter#android.intent.action.MAIN+android.intent.category.LAUNCHER
ADDED from AndroidManifest.xml:42:13
action#android.intent.action.MAIN
ADDED from AndroidManifest.xml:43:17
	android:name
		ADDED from AndroidManifest.xml:43:25
category#android.intent.category.LAUNCHER
ADDED from AndroidManifest.xml:45:17
	android:name
		ADDED from AndroidManifest.xml:45:27
activity#com.peakbw.loanapp.activities.EmployerActivity
ADDED from AndroidManifest.xml:48:9
	android:screenOrientation
		ADDED from AndroidManifest.xml:52:13
	android:label
		ADDED from AndroidManifest.xml:50:13
	android:parentActivityName
		ADDED from AndroidManifest.xml:51:13
	android:name
		ADDED from AndroidManifest.xml:49:13
meta-data#android.support.PARENT_ACTIVITY
ADDED from AndroidManifest.xml:53:13
	android:value
		ADDED from AndroidManifest.xml:55:17
	android:name
		ADDED from AndroidManifest.xml:54:17
activity#com.peakbw.loanapp.activities.EnrollmentActivity1
ADDED from AndroidManifest.xml:57:9
	android:screenOrientation
		ADDED from AndroidManifest.xml:61:13
	android:label
		ADDED from AndroidManifest.xml:59:13
	android:parentActivityName
		ADDED from AndroidManifest.xml:60:13
	android:name
		ADDED from AndroidManifest.xml:58:13
activity#com.peakbw.loanapp.activities.EnrollmentActivity2
ADDED from AndroidManifest.xml:66:9
	android:label
		ADDED from AndroidManifest.xml:68:13
	android:parentActivityName
		ADDED from AndroidManifest.xml:69:13
	android:uiOptions
		ADDED from AndroidManifest.xml:70:13
	android:name
		ADDED from AndroidManifest.xml:67:13
meta-data#android.support.UI_OPTIONS
ADDED from AndroidManifest.xml:74:13
	android:value
		ADDED from AndroidManifest.xml:76:17
	android:name
		ADDED from AndroidManifest.xml:75:17
activity#com.peakbw.loanapp.activities.ConfirmationActivity
ADDED from AndroidManifest.xml:78:9
	android:screenOrientation
		ADDED from AndroidManifest.xml:82:13
	android:label
		ADDED from AndroidManifest.xml:80:13
	android:parentActivityName
		ADDED from AndroidManifest.xml:81:13
	android:name
		ADDED from AndroidManifest.xml:79:13
activity#com.peakbw.loanapp.activities.VerificationActivity
ADDED from AndroidManifest.xml:87:9
	android:label
		ADDED from AndroidManifest.xml:89:13
	android:parentActivityName
		ADDED from AndroidManifest.xml:90:13
	android:name
		ADDED from AndroidManifest.xml:88:13
activity#com.peakbw.loanapp.activities.HistoryActivity
ADDED from AndroidManifest.xml:95:9
	android:screenOrientation
		ADDED from AndroidManifest.xml:99:13
	android:label
		ADDED from AndroidManifest.xml:97:13
	android:parentActivityName
		ADDED from AndroidManifest.xml:98:13
	android:name
		ADDED from AndroidManifest.xml:96:13
activity#com.peakbw.loanapp.activities.DetailsActivity
ADDED from AndroidManifest.xml:104:9
	android:screenOrientation
		ADDED from AndroidManifest.xml:108:13
	android:label
		ADDED from AndroidManifest.xml:106:13
	android:parentActivityName
		ADDED from AndroidManifest.xml:107:13
	android:name
		ADDED from AndroidManifest.xml:105:13
service#com.peakbw.loanapp.services.FTPService
ADDED from AndroidManifest.xml:114:9
	android:enabled
		ADDED from AndroidManifest.xml:116:13
	android:label
		ADDED from AndroidManifest.xml:119:13
	android:exported
		ADDED from AndroidManifest.xml:117:13
	android:icon
		ADDED from AndroidManifest.xml:118:13
	android:name
		ADDED from AndroidManifest.xml:115:13
receiver#com.peakbw.loanapp.receivers.BootReceiver
ADDED from AndroidManifest.xml:122:9
	android:enabled
		ADDED from AndroidManifest.xml:124:13
	android:exported
		ADDED from AndroidManifest.xml:125:13
	android:permission
		ADDED from AndroidManifest.xml:126:13
	android:name
		ADDED from AndroidManifest.xml:123:13
intent-filter#android.intent.action.BOOT_COMPLETED
ADDED from AndroidManifest.xml:127:13
action#android.intent.action.BOOT_COMPLETED
ADDED from AndroidManifest.xml:128:17
	android:name
		ADDED from AndroidManifest.xml:128:25
activity#com.peakbw.loanapp.activities.HistoryCollection
ADDED from AndroidManifest.xml:132:9
	android:screenOrientation
		ADDED from AndroidManifest.xml:136:13
	android:label
		ADDED from AndroidManifest.xml:134:13
	android:parentActivityName
		ADDED from AndroidManifest.xml:135:13
	android:name
		ADDED from AndroidManifest.xml:133:13
activity#com.peakbw.loanapp.activities.CollectionDetailActivity
ADDED from AndroidManifest.xml:141:9
	android:screenOrientation
		ADDED from AndroidManifest.xml:144:13
	android:label
		ADDED from AndroidManifest.xml:143:13
	android:name
		ADDED from AndroidManifest.xml:142:13
activity#com.peakbw.loanapp.activities.CameraActivity
ADDED from AndroidManifest.xml:146:9
	android:label
		ADDED from AndroidManifest.xml:148:13
	android:parentActivityName
		ADDED from AndroidManifest.xml:149:13
	android:name
		ADDED from AndroidManifest.xml:147:13
