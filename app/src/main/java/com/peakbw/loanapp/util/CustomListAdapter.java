package com.peakbw.loanapp.util;

import android.app.Activity;
import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import com.peakbw.loanapp.R;
import java.util.List;

/**
 * Created by root on 8/5/15.
 */
public class CustomListAdapter extends BaseAdapter {

    private Activity activity;
    private LayoutInflater inflater;
    private List<ListItem> listItems;
    public ImageLoader imageLoader;

    private static final String DEBURG_TAG = "CustomListAdapter";

    public CustomListAdapter(Activity activity, List<ListItem> listItems) {
        this.activity = activity;
        this.listItems = listItems;

        imageLoader=new ImageLoader(activity.getApplicationContext());
    }

    @Override
    public int getCount() {
        return listItems.size();
    }

    @Override
    public Object getItem(int location) {
        return listItems.get(location);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        if (inflater == null){
            inflater = (LayoutInflater) activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        }
        if (convertView == null){
            convertView = inflater.inflate(R.layout.list_row, null);
        }


        TextView employee = (TextView) convertView.findViewById(R.id.employee);
        TextView employer = (TextView) convertView.findViewById(R.id.employer);
        TextView date = (TextView) convertView.findViewById(R.id.date);
        ImageView pic = (ImageView) convertView.findViewById(R.id.list_image);
        ImageView state = (ImageView) convertView.findViewById(R.id.status);
        ImageView sendIcon = (ImageView) convertView.findViewById(R.id.sendIcon);

        // getting movie data for the row
        ListItem m = listItems.get(position);

        // thumbnail image
        //Uri uri = Uri.fromFile(new File(m.getThumbnailUrl()));
        //pic.setImageURI(uri);

        imageLoader.DisplayImage(m.getThumbnailUrl(), pic);

        // employee
        employee.setText(m.getEmployee());

        // employer
        employer.setText(m.getEmployer());

        // creation date
        String dateTime = m.getDate();
        date.setText(dateTime.substring(0,dateTime.length()-4));

        String status = m.getStatus();
        if(status.equals("1")){
            state.setImageResource(R.drawable.correct);
        }else if(status.equals("2")){
            state.setImageResource(R.drawable.cancel);
        }

        /*int tabIndex = m.getTabIndex();
        Log.d(DEBURG_TAG, "TAB INDEX ="+tabIndex);
        if(tabIndex == 1 || tabIndex == 2){
            sendIcon.setVisibility(View.VISIBLE);
            sendIcon.setFocusable(true);
        }*/

        return convertView;
    }
}
