package com.peakbw.loanapp.util;

/**
 * Created by root on 8/5/15.
 */
public class ListItem {
    private String employee;
    private String  employer;
    private String  status;
    private String date;
    private String thumbnailUrl;
    private String id;
    private int tabIndex;

    public ListItem() {
    }

    public String getEmployee(){
        return employee;
    }

    public void setEmployee(String employee){
        this.employee = employee;
    }

    public String getEmployer(){
        return employer;
    }

    public void setEmployer(String employer){
        this.employer = employer;
    }

    public String getThumbnailUrl() {
        return thumbnailUrl;
    }

    public void setThumbnailUrl(String thumbnailUrl) {
        this.thumbnailUrl = thumbnailUrl;
    }

    public String getDate(){
        return date;
    }

    public void setDate(String date){
        this.date = date;
    }

    public String getStatus(){
        return status;
    }

    public void setStatus(String status){
        this.status = status;
    }

    public String getId(){
        return id;
    }

    public void setId(String id){
        this.id = id;
    }

    public int getTabIndex(){
        return tabIndex;
    }

    public void setTabIndex(int tabIndex){
        this.tabIndex = tabIndex;
    }
}
