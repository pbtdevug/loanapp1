package com.peakbw.loanapp.activities;

import android.app.ActionBar;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.app.NavUtils;
import android.support.v4.app.TaskStackBuilder;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.peakbw.loanapp.R;
import com.peakbw.loanapp.db.ReaderContract;
import com.peakbw.loanapp.db.ReaderDBHelper;
import com.peakbw.loanapp.net.ManageServerConnections;
import com.peakbw.loanapp.net.XMLParser;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;

import static com.peakbw.loanapp.activities.ConfirmationActivity.updateStateTable;

public class CollectionDetailActivity extends FragmentActivity {

    private static ReaderDBHelper rDbHelper;
    private static String state,record_ts;
    private static int itemPsn;
    private static Cursor cursor;
    private static String recordState,staffName,staffId,employerId,employerName,dob,phoneNum,maxInstallment,netSalary;
    private  ManageServerConnections mdc;

    private static String idFile = "0",consentFile = "0" ,photoFile = "0";

    private String veriXML;

    public static final int DIALOG_REQUEST_PROGRESS = 0;
    public static final int DIALOG_VERIFICATION = 1;
    private static  final String DEBURG_TAG = "CollectionDetailActivity";

    private AlertDialog.Builder vericationDialog;
    public static ProgressDialog progressDialog;

    private CollectionPagerAdapter collectionPagerAdapter;
    private ViewPager mViewPager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_collection_detail);

        final ActionBar actionBar = getActionBar();
        actionBar.setDisplayHomeAsUpEnabled(true);

        mdc = new ManageServerConnections();
    }

    @Override
    protected void onStart(){
        super.onStart();

        rDbHelper = new ReaderDBHelper(this);

        Intent intent = getIntent();

        itemPsn = intent.getIntExtra(HistoryCollection.ITEM_PSN,0);
        state = intent.getStringExtra(HistoryCollection.RECORD_STATE);
        record_ts = intent.getStringExtra(HistoryCollection.RECORD_TS);

        collectionPagerAdapter = new CollectionPagerAdapter(getSupportFragmentManager());

        // Set up the ViewPager, attaching the adapter.
        mViewPager = (ViewPager) findViewById(R.id.pager);
        mViewPager.setAdapter(collectionPagerAdapter);
        mViewPager.setCurrentItem(itemPsn);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        cursor.close();
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.collection_detail, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                // This is called when the Home (Up) button is pressed in the action bar.
                // Create a simple intent that starts the hierarchical parent activity and
                // use NavUtils in the Support Package to ensure proper handling of Up.
                Intent upIntent = new Intent(this, MainActivity.class);
                if (NavUtils.shouldUpRecreateTask(this, upIntent)) {
                    // This activity is not part of the application's task, so create a new task
                    // with a synthesized back stack.
                    TaskStackBuilder.from(this)
                            // If there are ancestor activities, they should be added here.
                            .addNextIntent(upIntent)
                            .startActivities();
                    finish();
                } else {
                    // This activity is part of the application's task, so simply
                    // navigate up to the hierarchical parent activity.
                    NavUtils.navigateUpTo(this, upIntent);
                }
                return true;
            case R.id.action_send:
                if(recordState.equals(EnrollmentActivity2.STATE_VERIFICATION)){
                    veriXML = createVerificationXML(staffId,employerId);
                    showDialog(DIALOG_VERIFICATION);
                }
                else if(recordState.equals(EnrollmentActivity2.STATE_ENROLLMENT)){
                    startEnrollmentActivity1();
                }
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    public static class CollectionPagerAdapter extends FragmentStatePagerAdapter {

        public CollectionPagerAdapter(FragmentManager fm) {
            super(fm);
            cursor = rDbHelper.getHistoryTrxns(state);
        }

        @Override
        public Fragment getItem(int i) {
            Fragment fragment = new ObjectFragment();
            Bundle args = new Bundle();
            args.putInt(ObjectFragment.ARG_OBJECT, i); // Our object is just an integer :-P
            fragment.setArguments(args);
            return fragment;
        }

        @Override
        public int getCount() {
            int count = cursor.getCount();
            return count;
        }

        @Override
        public CharSequence getPageTitle(int position) {
            cursor.moveToPosition(position);
            return cursor.getString(cursor.getColumnIndexOrThrow(ReaderContract.State.TIME_STAMP));
        }
    }

    public static class ObjectFragment extends Fragment {

        public static final String ARG_OBJECT = "object";

        @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container,Bundle savedInstanceState) {
            View rootView = inflater.inflate(R.layout.fragment_details_object, container, false);

            TextView employee = (TextView) rootView.findViewById(R.id.nameValue);
            TextView employeeId = (TextView) rootView.findViewById(R.id.idValue);
            TextView employer = (TextView) rootView.findViewById(R.id.empValue);
            TextView hireDate = (TextView) rootView.findViewById(R.id.hireValue);

            TextView birthDate = (TextView) rootView.findViewById(R.id.dobValue);
            TextView sex = (TextView) rootView.findViewById(R.id.genderValue);
            TextView simNumber = (TextView) rootView.findViewById(R.id.phoneValue);
            TextView salary = (TextView) rootView.findViewById(R.id.salaryValue);
            TextView installment = (TextView) rootView.findViewById(R.id.installValue);

            Bundle args = getArguments();
            int recordPsn = args.getInt(ARG_OBJECT);

            cursor.moveToPosition(recordPsn);
            staffName = cursor.getString(cursor.getColumnIndexOrThrow(ReaderContract.Employee.EMPLOYEE_NAME));
            employee.setText(staffName);
            staffId = cursor.getString(cursor.getColumnIndexOrThrow(ReaderContract.Employee.EMPLOYEE_ID));
            employeeId.setText(staffId);

            employerName = cursor.getString(cursor.getColumnIndexOrThrow(ReaderContract.Employer.EMPLOYER_NAME));
            employer.setText(employerName);
            employerId = cursor.getString(cursor.getColumnIndexOrThrow(ReaderContract.Employee.EMPLOYER_ID));

            dob = cursor.getString(cursor.getColumnIndexOrThrow(ReaderContract.State.DOB));
            birthDate.setText(dob);
            String gender = cursor.getString(cursor.getColumnIndexOrThrow(ReaderContract.State.GENDER));
            sex.setText(gender);
            phoneNum = cursor.getString(cursor.getColumnIndexOrThrow(ReaderContract.State.PHONE_NUMBER));
            simNumber.setText(phoneNum);
            String doa = cursor.getString(cursor.getColumnIndexOrThrow(ReaderContract.State.HIRING_DATE));
            hireDate.setText(doa);
            netSalary = cursor.getString(cursor.getColumnIndexOrThrow(ReaderContract.State.NET_SALARY));
            salary.setText(netSalary);
            maxInstallment = cursor.getString(cursor.getColumnIndexOrThrow(ReaderContract.State.BORROWING_LIMIT));
            installment.setText(maxInstallment);

            recordState = cursor.getString(cursor.getColumnIndexOrThrow(ReaderContract.State.STATUS));

            //idFile = cursor.getString(cursor.getColumnIndexOrThrow(ReaderContract.Pics.));

            return rootView;
        }
    }

    private String createVerificationXML(String staffId,String employerId){

        MainActivity.activeEmployee = staffName+"-"+staffId;

        StringBuffer xmlStr = new StringBuffer();
        xmlStr.append("<?xml version='1.0' encoding='UTF-8'?>");
        xmlStr.append("<pbtRequest>");
        xmlStr.append("<method>").append("faircap_verification").append("</method>");
        xmlStr.append("<pin>").append(MainActivity.pin).append("</pin>");
        xmlStr.append("<imei>").append(MainActivity.phoneID).append("</imei>");
        xmlStr.append("<msisdn>").append(MainActivity.simSerialNumber).append("</msisdn>");
        xmlStr.append("<employeeID>").append(staffId).append("</employeeID>");
        xmlStr.append("<employerID>").append(employerId).append("</employerID>");
        Log.d(DEBURG_TAG, "veriXML = " + xmlStr.toString());
        return xmlStr.toString();
    }

    @Override
    protected Dialog onCreateDialog(int id) {
        switch (id) {
            case DIALOG_REQUEST_PROGRESS:
                progressDialog = new ProgressDialog(this);
                progressDialog.setMessage("please wait");
                progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
                progressDialog.setCancelable(true);
                progressDialog.show();
                return progressDialog;

            case DIALOG_VERIFICATION:
                vericationDialog = new AlertDialog.Builder(this);
                LayoutInflater li = LayoutInflater.from(this);
                View promptsView = li.inflate(R.layout.verification_prompt, null);
                final EditText veriInput = (EditText) promptsView.findViewById(R.id.verification_code);
                vericationDialog.setView(promptsView);
                vericationDialog.setCancelable(false);
                vericationDialog.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    // do something when the button is clicked
                    public void onClick(DialogInterface dialog, int id) {
                        String veriCode = veriInput.getText().toString();
                        if(veriCode.length()==6){
                            veriXML = veriXML+"<code>"+veriCode+"</code>";
                            veriXML = veriXML+"</pbtRequest>";
                            Log.d(DEBURG_TAG, veriXML);
                            mdc.setUpdateVariable(veriXML);
                            new ConnectionTask().execute(MainActivity.url);
                        }
                        else{
                            Toast.makeText(CollectionDetailActivity.this, "Code must be 6 digits!", Toast.LENGTH_LONG).show();
                        }
                    }
                });
                vericationDialog.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int id) {
                    }
                });
                return vericationDialog.create();

            default:
                return null;
        }
    }

    private void createResponseDialog(String msg,int layout,int textView){
        LayoutInflater li = LayoutInflater.from(this);
        View promptsView = li.inflate(layout, null);
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);
        alertDialogBuilder.setView(promptsView);

        final TextView successTextView = (TextView) promptsView.findViewById(textView);
        successTextView.setText(msg);

        // set dialog message
        alertDialogBuilder.setCancelable(false).setPositiveButton("OK",new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog,int id) {
                //finish();
            }
        });

        AlertDialog alertDialog = alertDialogBuilder.create();
        alertDialog.show();
    }

    public class ConnectionTask extends AsyncTask<String, Void, String> {
        private String status = "";
        private InputStream stream = null;
        private static final String DEBUG_TAG = "DownloadXMLTask";

        @SuppressWarnings("deprecation")
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            showDialog(DIALOG_REQUEST_PROGRESS);
        }

        @Override
        protected String doInBackground(String... url) {
            try {
                stream = mdc.downloadUrl(url[0]);
                try	{
                    status = XMLParser.parseXML(stream);
                }
                catch(Exception ex){
                    ex.printStackTrace();
                }
            }
            catch(IOException ex){
                ex.printStackTrace();
            }
            finally {
                if (stream != null) {
                    try{
                        stream.close();
                    }catch(IOException ex){

                    }
                }
            }
            return status;
        }

        @SuppressWarnings("deprecation")
        @Override
        protected void onPostExecute(String result) {
            dismissDialog(DIALOG_REQUEST_PROGRESS);
            Log.d(DEBUG_TAG,"POST EXECUTE");
            if(result!=null){
                Log.d(DEBUG_TAG, result);
                if(result.equals("SUCCESSFUL")&&XMLParser.method.equals("faircap_verification")){
                    updateStateTable(EnrollmentActivity2.STATE_COMPLETE);
                    createResponseDialog(result,R.layout.success_prompt,R.id.success);
                }
                else{
                    if(ManageServerConnections.timeout==null){
                        createResponseDialog(result,R.layout.error_prompt,R.id.error);
                    }
                    else{
                        createResponseDialog(ManageServerConnections.timeout,R.layout.error_prompt,R.id.error);
                    }
                }
            }else{
                createResponseDialog("Invalid Response",R.layout.error_prompt,R.id.error);
            }
        }
    }

    private void startEnrollmentActivity1(){

        Intent enrollIntent = new Intent(this,EnrollmentActivity1.class);
        enrollIntent.putExtra(EmployerActivity.EMPLOYEE,staffName);
        enrollIntent.putExtra(EmployerActivity.EMPLOYEE_ID,staffId);
        enrollIntent.putExtra(EmployerActivity.EMPLOYER,employerName);
        enrollIntent.putExtra(EmployerActivity.EMPLOYER_ID,employerId);
        enrollIntent.putExtra(EmployerActivity.DOB,dob);
        enrollIntent.putExtra(EmployerActivity.PHONE,phoneNum);
        enrollIntent.putExtra(EmployerActivity.BORROWING_LIMIT,maxInstallment);
        enrollIntent.putExtra(EmployerActivity.NET_SALARY,netSalary);

        enrollIntent.putExtra(EmployerActivity.ID_FILE,idFile);
        enrollIntent.putExtra(EmployerActivity.CONSENT_FILE,consentFile);
        enrollIntent.putExtra(EmployerActivity.PHOTO_FILE,photoFile);

        System.out.println(idFile);
        System.out.println(consentFile);
        System.out.println(photoFile);

        enrollIntent.putExtra(EmployerActivity.ID_SCANNED,"false");
        enrollIntent.putExtra(EmployerActivity.CONSENT_SCANNED,"false");
        enrollIntent.putExtra(EmployerActivity.PHOTO_CAPTURED,"false");

        startActivity(enrollIntent);
    }
}
