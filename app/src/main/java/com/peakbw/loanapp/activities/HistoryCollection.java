package com.peakbw.loanapp.activities;

import android.app.ActionBar;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.view.View;

import com.peakbw.loanapp.R;
import com.peakbw.loanapp.fragments.HistroySectionFragment;


public class HistoryCollection extends FragmentActivity implements ActionBar.TabListener,HistroySectionFragment.OnFragmentInteractionListener {

    /**
     * The PagerAdapter that will provide fragments for each of the
     * three primary sections of the app. We use a FragmentPagerAdapter
     * derivative, which will keep every loaded fragment in memory. If this becomes too memory
     * intensive, it may be best to switch to a android.support.v4.app.FragmentStatePagerAdapter.
     */

    private  AppSectionsPagerAdapter mAppSectionsPagerAdapter;

    /**
     * The ViewPager that will display the three primary sections of the app, one at a
     * time.
     */
    private ViewPager mViewPager;

    private static final String DEBURG_TAG = "HistoryCollections";
    public static  final String RECORD_TS = "ts";
    public static  final String RECORD_STATE = "state";
    public static  final String ITEM_PSN = "psn";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_history_collection);

        // Create the adapter that will return a fragment for each of the three primary sections
        // of the app.
        mAppSectionsPagerAdapter = new AppSectionsPagerAdapter(getSupportFragmentManager());

        // Set up the action bar.
        final ActionBar actionBar = getActionBar();

        // Specify that the Home/Up button should not be enabled, since there is no hierarchical
        // parent.
        actionBar.setHomeButtonEnabled(false);

        // Specify that we will be displaying tabs in the action bar.
        actionBar.setNavigationMode(ActionBar.NAVIGATION_MODE_TABS);

        // Set up the ViewPager, attaching the adapter and setting up a listener for when the
        // user swipes between sections.
        mViewPager = (ViewPager) findViewById(R.id.pager);
        mViewPager.setAdapter(mAppSectionsPagerAdapter);
        mViewPager.setOnPageChangeListener(new ViewPager.SimpleOnPageChangeListener() {
            @Override
            public void onPageSelected(int position) {
                // When swiping between different app sections, select the corresponding tab.
                // We can also use ActionBar.Tab#select() to do this if we have a reference to the
                // Tab.
                actionBar.setSelectedNavigationItem(position);
            }
        });

        // For each of the sections in the app, add a tab to the action bar.
        for (int i = 0; i < mAppSectionsPagerAdapter.getCount(); i++) {
            // Create a tab with text corresponding to the page title defined by the adapter.
            // Also specify this Activity object, which implements the TabListener interface, as the
            // listener for when this tab is selected.
            actionBar.addTab(actionBar.newTab().setText(mAppSectionsPagerAdapter.getPageTitle(i)).setTabListener(this));
        }

    }


    /*@Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.history_collection, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        if (id == R.id.action_settings) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }*/

    @Override
    public void onTabSelected(ActionBar.Tab tab, android.app.FragmentTransaction fragmentTransaction) {
        // When the given tab is selected, switch to the corresponding page in the ViewPager.
        mViewPager.setCurrentItem(tab.getPosition());
    }

    @Override
    public void onTabUnselected(ActionBar.Tab tab, android.app.FragmentTransaction fragmentTransaction) {

    }

    @Override
    public void onTabReselected(ActionBar.Tab tab, android.app.FragmentTransaction fragmentTransaction) {

    }

    @Override
    public void onFragmentInteraction(String ts,String state,int itemPosition) {
        Log.d(DEBURG_TAG,"ITEM ID = "+ts+"\nRecord State = "+state+"\nItem Position = "+itemPosition);
        Intent detailsInt = new Intent(this,CollectionDetailActivity.class);
        detailsInt.putExtra(RECORD_TS,ts);
        detailsInt.putExtra(RECORD_STATE,state);
        detailsInt.putExtra(ITEM_PSN,itemPosition);
        startActivity(detailsInt);
    }

    public void onClickListener(View view){
        Log.d(DEBURG_TAG,"ImageView Clicked");
    }

    public class AppSectionsPagerAdapter extends FragmentPagerAdapter {

        public AppSectionsPagerAdapter(FragmentManager fm) {

            super(fm);
        }

        @Override
        public Fragment getItem(int sectionIndex) {

            Fragment fragment = HistroySectionFragment.newInstance(getBaseContext());
            Bundle args = new Bundle();
            args.putInt(HistroySectionFragment.ARG_PARAM1, sectionIndex);
            fragment.setArguments(args);
            return fragment;

        }

        @Override
        public int getCount() {
            return 4;
        }

        @Override
        public CharSequence getPageTitle(int position) {
            if (position == 0) {
                return "Complete";
            } else if (position == 1) {
                return "Not Verified";
            } else if (position == 2) {
                return "Not Enrolled";
            } else {
                return "Documents";
            }

        }
    }

}
