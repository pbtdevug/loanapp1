package com.peakbw.loanapp.activities;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.List;
import java.util.Map;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ListActivity;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.SimpleAdapter;
import android.widget.TextView;
import android.widget.Toast;

import com.peakbw.loanapp.R;
import com.peakbw.loanapp.db.ReaderContract.Employee;
import com.peakbw.loanapp.db.ReaderContract.Employer;
import com.peakbw.loanapp.db.ReaderContract.State;
import com.peakbw.loanapp.db.ReaderDBHelper;
import com.peakbw.loanapp.net.ManageServerConnections;
import com.peakbw.loanapp.net.XMLParser;

import static com.peakbw.loanapp.activities.ConfirmationActivity.updateStateTable;

public class HistoryActivity extends ListActivity {
	private SimpleAdapter adapter;
	private Hashtable<String, String> historyHash;
	private static final String DEBUG_TAG = "HistoryActivity";
    private  ManageServerConnections mdc;
    public static final int DIALOG_REQUEST_PROGRESS = 0;
    public static final int DIALOG_VERIFICATION = 1;
    private AlertDialog.Builder vericationDialog;
    private String veriXML;
    public static ProgressDialog progressDialog;
	public static final String activityKey = "com.peakbw.loanapp.activities.HistoryActivity";
    private ReaderDBHelper rDbHelper;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_history);
		// Show the Up button in the action bar.
		setupActionBar();
		
		setTitle("History");

        rDbHelper = new ReaderDBHelper(getBaseContext());
		
		historyHash = new Hashtable<String, String>();

        mdc = new ManageServerConnections();
	}

	/**
	 * Set up the {@link android.app.ActionBar}.
	 */
	private void setupActionBar() {
		getActionBar().setDisplayHomeAsUpEnabled(true);
	}
	
	@Override 
	public void onListItemClick(ListView l, View v, int position, long id) {
		@SuppressWarnings("unchecked")
		HashMap<String, String> histMap = (HashMap<String, String>)l.getItemAtPosition(position);
		String selectedHist = histMap.get("employee");
		String status = histMap.get("state");
		Log.d(DEBUG_TAG, "Selected Item "+selectedHist);
		Log.d(DEBUG_TAG, "state "+status);
		if(status.equals(EnrollmentActivity2.STATE_VERIFICATION)){
			//Intent veriIntent = new Intent(this,VerificationActivity.class);
			//veriIntent.putExtra(ConfirmationActivity.activityKey, createVerificationXML(selectedHist));
			//startActivity(veriIntent);
            veriXML = createVerificationXML(selectedHist);
            showDialog(DIALOG_VERIFICATION);
		}
		else if(status.equals(EnrollmentActivity2.STATE_DOCUMENTS)){
			startEnrollmentActivity1(selectedHist);
			//retrieveEmployeeDetails(selectedHist);
		}
		else if(status.equals(EnrollmentActivity2.STATE_ENROLLMENT)){
			startEnrollmentActivity1(selectedHist);
			/*Intent confirmationIntent = new Intent(this,ConfirmationActivity.class);
			confirmationIntent.putExtra(EnrollmentActivity2.activityKey, getEnrollmentInfo(selectedHist));
			startActivity(confirmationIntent);*/
		}
		else if(status.equals(EnrollmentActivity2.STATE_COMPLETE)){
			Intent detailsIntent = new Intent(this,DetailsActivity.class);
			detailsIntent.putExtra(activityKey, getEnrollmentInfo(selectedHist));
			startActivity(detailsIntent);
		}
    }
	
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		switch (item.getItemId()) {
		case android.R.id.home:
			Log.d(DEBUG_TAG, "Up button clicked!");
			finish();
			return true;
    	}
		
		return super.onOptionsItemSelected(item);
	}
	
	@Override
    protected void onStart() {
        super.onStart();
        
        Cursor cursor = rDbHelper.getHistory();
        
        //ArrayList<String> historyList = new ArrayList<String>();
        List<Map<String, String>> historyList = new ArrayList<Map<String, String>>();
        
        while(cursor.moveToNext()){
        	Map<String, String> datum = new HashMap<String, String>(2);
        	String name = cursor.getString(cursor.getColumnIndexOrThrow(Employee.EMPLOYEE_NAME));
        	String id = cursor.getString(cursor.getColumnIndexOrThrow(Employee.EMPLOYEE_ID));
        	String status = cursor.getString(cursor.getColumnIndexOrThrow(State.STATUS));
        	String employee = name+"-"+id;
        	
        	datum.put("employee", employee);
            datum.put("state", status);
            historyHash.put(employee, status);
            historyList.add(datum);
        }
        
        adapter = new SimpleAdapter(this, historyList,R.layout.list_elements,
                new String[] {"employee", "state"},new int[] {R.id.item,R.id.sub_item});
        //adapter = new ArrayAdapter<String>(this, R.layout.list_elements, R.id.item, historyList);
        
        setListAdapter(adapter);
    }
	
	@Override
    protected void onResume() {
        super.onResume();
        // The activity has become visible (it is now "resumed").
    }
    @Override
    protected void onPause() {
        super.onPause();
    }
    @Override
    protected void onStop() {
        super.onStop();
        // The activity is no longer visible (it is now "stopped")
    }
    @Override
    protected void onDestroy() {
        super.onDestroy();
        // The activity is about to be destroyed.
    }
    
    private String createVerificationXML(String histItem){
    	//split selected History to get employee ID and Name
    	ArrayList<String> employeeDets = ManageServerConnections.split(histItem, "-");
    	//execute query to get employer ID
    	Cursor cursor = rDbHelper.getEmployerID(employeeDets.get(1), employeeDets.get(0));
		cursor.moveToFirst();
		String employerID = cursor.getString(cursor.getColumnIndexOrThrow(Employee.EMPLOYER_ID));
    	
		MainActivity.activeEmployee = histItem;
		
    	StringBuffer xmlStr = new StringBuffer();
        xmlStr.append("<?xml version='1.0' encoding='UTF-8'?>");
        xmlStr.append("<pbtRequest>");
        xmlStr.append("<method>").append("faircap_verification").append("</method>");
        xmlStr.append("<pin>").append(MainActivity.pin).append("</pin>");
        xmlStr.append("<imei>").append(MainActivity.phoneID).append("</imei>");
        xmlStr.append("<msisdn>").append(MainActivity.simSerialNumber).append("</msisdn>");
        xmlStr.append("<employeeID>").append(employeeDets.get(1)).append("</employeeID>");
        xmlStr.append("<employerID>").append(employerID).append("</employerID>");
        Log.d(DEBUG_TAG, "veriXML = "+xmlStr.toString());
        return xmlStr.toString();
    }
    
    private void retrieveEmployeeDetails(String selectHistoryItem){
    	//split selected History to get employee ID and Name
    	ArrayList<String> employeeDets = ManageServerConnections.split(selectHistoryItem, "-");
    	//execute query to get employer ID
    	Cursor cursor = rDbHelper.getEmployerID(employeeDets.get(1), employeeDets.get(0));
		cursor.moveToFirst();
		String employerID = cursor.getString(cursor.getColumnIndexOrThrow(Employee.EMPLOYER_ID));
    	
		String employeeID = employeeDets.get(1);
		//query to get employee details
    	Cursor c = rDbHelper.getEmployeeHistory(employeeID, employerID);
		c.moveToFirst();
		
		String employer = c.getString(c.getColumnIndexOrThrow(Employer.EMPLOYER_NAME));
		String employee = c.getString(c.getColumnIndexOrThrow(Employee.EMPLOYEE_NAME));
		String dob = c.getString(c.getColumnIndexOrThrow(State.DOB));
		String gender = c.getString(c.getColumnIndexOrThrow(State.GENDER));
		String phone = c.getString(c.getColumnIndexOrThrow(State.PHONE_NUMBER));
		String limit = c.getString(c.getColumnIndexOrThrow(State.BORROWING_LIMIT));
		String salary = c.getString(c.getColumnIndexOrThrow(State.NET_SALARY));
		
		Intent enrollment2Intent = new Intent(this,EnrollmentActivity2.class);
		enrollment2Intent.putExtra(EmployerActivity.ID_FILE,"0");
    	enrollment2Intent.putExtra(EmployerActivity.CONSENT_FILE,"0");
    	enrollment2Intent.putExtra(EmployerActivity.PHOTO_FILE,"0");
    	enrollment2Intent.putExtra(EmployerActivity.ID_SCANNED,"false");
    	enrollment2Intent.putExtra(EmployerActivity.CONSENT_SCANNED,"false");
    	enrollment2Intent.putExtra(EmployerActivity.PHOTO_CAPTURED,"false");
		
		
		String employeeInfo = employer+"*"+employee+"*"+dob+"*"+gender+"*"+phone+"*"+limit+"*"+salary+"*"+employerID+"*"+employeeID;
		
		enrollment2Intent.putExtra(EnrollmentActivity1.activityKey, employeeInfo); 
		
		Log.d(DEBUG_TAG, employeeInfo);
		
		startActivity(enrollment2Intent);
    }
    
    private String getEnrollmentInfo(String selectedHistory){
    	//split selected History to get employee ID and Name
    	ArrayList<String> employeeDets = ManageServerConnections.split(selectedHistory, "-");
    	//execute query to get employer ID
    	Cursor cursor = rDbHelper.getEmployerID(employeeDets.get(1), employeeDets.get(0));
		cursor.moveToFirst();
		String employerID = cursor.getString(cursor.getColumnIndexOrThrow(Employee.EMPLOYER_ID));
    	
		String employeeID = employeeDets.get(1);
		//query to get employee details
    	Cursor c = rDbHelper.getEnrollmentInfo(employeeID, employerID);
		c.moveToFirst();
		
		String employer = c.getString(c.getColumnIndexOrThrow(Employer.EMPLOYER_NAME));
		String employee = c.getString(c.getColumnIndexOrThrow(Employee.EMPLOYEE_NAME));
		String dob = c.getString(c.getColumnIndexOrThrow(State.DOB));
		String gender = c.getString(c.getColumnIndexOrThrow(State.GENDER));
		String phone = c.getString(c.getColumnIndexOrThrow(State.PHONE_NUMBER));
		String limit = c.getString(c.getColumnIndexOrThrow(State.BORROWING_LIMIT));
		String salary = c.getString(c.getColumnIndexOrThrow(State.NET_SALARY));
		String isIdScanned = c.getString(c.getColumnIndexOrThrow(State.CAPTURED_ID));
		String isConsentScanned = c.getString(c.getColumnIndexOrThrow(State.CAPTURED_CONSENT));
		String isPhotoCaptured = c.getString(c.getColumnIndexOrThrow(State.CAPTURED_PHOTO));
		String idFile = c.getString(c.getColumnIndexOrThrow(State.ID_FILE));
		String consentFile = c.getString(c.getColumnIndexOrThrow(State.CONSENT_FILE));
		String photoFile = c.getString(c.getColumnIndexOrThrow(State.PHOTO_FILE));
		
		if(isIdScanned.equals("1")){
			isIdScanned = "true";
		}
		else{
			isIdScanned = "false";
		}
		
		if(isConsentScanned.equals("1")){
			isConsentScanned = "true";
		}
		else{
			isConsentScanned = "false";
		}
		
		if(isPhotoCaptured.equals("1")){
			isPhotoCaptured = "true";
		}
		else{
			isPhotoCaptured = "false";
		}
    	
    	String enrollmentInfo = employer+"*"+employee+"*"+dob+"*"+gender+"*"+phone+"*"+limit+"*"+
    			salary+"*"+employerID+"*"+employeeID+"*"+isIdScanned+"*"+isConsentScanned+"*"+
    			isPhotoCaptured+"*"+idFile+"*"+"*"+consentFile+"*"+photoFile;
    	
    	Log.d(DEBUG_TAG, enrollmentInfo);
    	
    	return enrollmentInfo;
    }
    
    private void startEnrollmentActivity1(String selectedHist){
    	
    	//split selected History to get employee ID and Name
    	ArrayList<String> employeeDets = ManageServerConnections.split(selectedHist, "-");
    	//execute query to get employer ID
    	Cursor cursor = rDbHelper.getEmployerID(employeeDets.get(1), employeeDets.get(0));
		cursor.moveToFirst();
		String employerID = cursor.getString(cursor.getColumnIndexOrThrow(Employee.EMPLOYER_ID));
    	
		String employeeID = employeeDets.get(1);
		//query to get employee details
    	Cursor c = rDbHelper.getEnrollmentInfo(employeeID, employerID);
		c.moveToFirst();
		
		String employer = c.getString(c.getColumnIndexOrThrow(Employer.EMPLOYER_NAME));
		String employee = c.getString(c.getColumnIndexOrThrow(Employee.EMPLOYEE_NAME));
		String dob = c.getString(c.getColumnIndexOrThrow(State.DOB));
		//String gender = c.getString(c.getColumnIndexOrThrow(State.GENDER));
		String phone = c.getString(c.getColumnIndexOrThrow(State.PHONE_NUMBER));
		String limit = c.getString(c.getColumnIndexOrThrow(State.BORROWING_LIMIT));
		String salary = c.getString(c.getColumnIndexOrThrow(State.NET_SALARY));
		String isIdScanned = c.getString(c.getColumnIndexOrThrow(State.CAPTURED_ID));
		String isConsentScanned = c.getString(c.getColumnIndexOrThrow(State.CAPTURED_CONSENT));
		String isPhotoCaptured = c.getString(c.getColumnIndexOrThrow(State.CAPTURED_PHOTO));
		String idFile = c.getString(c.getColumnIndexOrThrow(State.ID_FILE));
		String consentFile = c.getString(c.getColumnIndexOrThrow(State.CONSENT_FILE));
		String photoFile = c.getString(c.getColumnIndexOrThrow(State.PHOTO_FILE));
		
		if(isIdScanned.equals("1")){
			isIdScanned = "true";
		}
		else{
			isIdScanned = "false";
		}
		
		if(isConsentScanned.equals("1")){
			isConsentScanned = "true";
		}
		else{
			isConsentScanned = "false";
		}
		
		if(isPhotoCaptured.equals("1")){
			isPhotoCaptured = "true";
		}
		else{
			isPhotoCaptured = "false";
		}
    	
		Intent enrollIntent = new Intent(this,EnrollmentActivity1.class);
		enrollIntent.putExtra(EmployerActivity.EMPLOYEE,employee);
		enrollIntent.putExtra(EmployerActivity.EMPLOYEE_ID,employeeID);
		enrollIntent.putExtra(EmployerActivity.EMPLOYER,employer);
		enrollIntent.putExtra(EmployerActivity.EMPLOYER_ID,employerID);
		enrollIntent.putExtra(EmployerActivity.DOB,dob);
		enrollIntent.putExtra(EmployerActivity.PHONE,phone);
		enrollIntent.putExtra(EmployerActivity.BORROWING_LIMIT,limit);
		enrollIntent.putExtra(EmployerActivity.NET_SALARY,salary);
		
		enrollIntent.putExtra(EmployerActivity.ID_FILE,idFile);
		enrollIntent.putExtra(EmployerActivity.CONSENT_FILE,consentFile);
		enrollIntent.putExtra(EmployerActivity.PHOTO_FILE,photoFile);

        System.out.println(idFile);
        System.out.println(consentFile);
        System.out.println(photoFile);
		
		enrollIntent.putExtra(EmployerActivity.ID_SCANNED,isIdScanned);
		enrollIntent.putExtra(EmployerActivity.CONSENT_SCANNED,isConsentScanned);
		enrollIntent.putExtra(EmployerActivity.PHOTO_CAPTURED,isPhotoCaptured);
		
		startActivity(enrollIntent);
	}

    public class ConnectionTask extends AsyncTask<String, Void, String> {
        private String status = "";
        private InputStream stream = null;
        private static final String DEBUG_TAG = "DownloadXMLTask";

        @SuppressWarnings("deprecation")
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            showDialog(DIALOG_REQUEST_PROGRESS);
        }

        @Override
        protected String doInBackground(String... url) {
            try {
                stream = mdc.downloadUrl(url[0]);
                try	{
                    status = XMLParser.parseXML(stream);
                }
                catch(Exception ex){
                    ex.printStackTrace();
                }
            }
            catch(IOException ex){
                ex.printStackTrace();
            }
            finally {
                if (stream != null) {
                    try{
                        stream.close();
                    }catch(IOException ex){

                    }
                }
            }
            return status;
        }

        @SuppressWarnings("deprecation")
        @Override
        protected void onPostExecute(String result) {
            dismissDialog(DIALOG_REQUEST_PROGRESS);
            Log.d(DEBUG_TAG,"POST EXECUTE");
            if(result!=null){
                Log.d(DEBUG_TAG, result);
                if(result.equals("SUCCESSFUL")&&XMLParser.method.equals("faircap_verification")){
                    updateStateTable(EnrollmentActivity2.STATE_COMPLETE);
                    createResponseDialog(result,R.layout.success_prompt,R.id.success);
                }
                else{
                    if(ManageServerConnections.timeout==null){
                        createResponseDialog(result,R.layout.error_prompt,R.id.error);
                    }
                    else{
                        createResponseDialog(ManageServerConnections.timeout,R.layout.error_prompt,R.id.error);
                    }
                }
            }else{
                createResponseDialog("Invalid Response",R.layout.error_prompt,R.id.error);
            }
        }
    }

    @Override
    protected Dialog onCreateDialog(int id) {
        switch (id) {
            case DIALOG_REQUEST_PROGRESS:
                progressDialog = new ProgressDialog(this);
                progressDialog.setMessage("please wait");
                progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
                progressDialog.setCancelable(true);
                progressDialog.show();
                return progressDialog;

            case DIALOG_VERIFICATION:
                vericationDialog = new AlertDialog.Builder(this);
                LayoutInflater li = LayoutInflater.from(this);
                View promptsView = li.inflate(R.layout.verification_prompt, null);
                final EditText veriInput = (EditText) promptsView.findViewById(R.id.verification_code);
                vericationDialog.setView(promptsView);
                vericationDialog.setCancelable(false);
                vericationDialog.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    // do something when the button is clicked
                    public void onClick(DialogInterface dialog, int id) {
                        String veriCode = veriInput.getText().toString();
                        if(veriCode.length()==6){
                            veriXML = veriXML+"<code>"+veriCode+"</code>";
                            veriXML = veriXML+"</pbtRequest>";
                            Log.d(DEBUG_TAG, veriXML);
                            mdc.setUpdateVariable(veriXML);
                            new ConnectionTask().execute(MainActivity.url);
                        }
                        else{
                            Toast.makeText(HistoryActivity.this, "Code must be 6 digits!", Toast.LENGTH_LONG).show();
                        }
                    }
                });
                vericationDialog.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int id) {
                    }
                });
                return vericationDialog.create();

            default:
                return null;
        }
    }

    private void createResponseDialog(String msg,int layout,int textView){
        LayoutInflater li = LayoutInflater.from(this);
        View promptsView = li.inflate(layout, null);
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);
        alertDialogBuilder.setView(promptsView);

        final TextView successTextView = (TextView) promptsView.findViewById(textView);
        successTextView.setText(msg);

        // set dialog message
        alertDialogBuilder.setCancelable(false).setPositiveButton("OK",new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog,int id) {
                //finish();
            }
        });

        AlertDialog alertDialog = alertDialogBuilder.create();
        alertDialog.show();
    }

}
