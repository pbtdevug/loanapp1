package com.peakbw.loanapp.activities;

import java.io.IOException;
import java.io.InputStream;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;
import com.peakbw.loanapp.R;
import com.peakbw.loanapp.net.ManageServerConnections;
import com.peakbw.loanapp.net.XMLParser;

public class VerificationActivity extends Activity {
	private String veriXML;
	private  ManageServerConnections mdc;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_verification);
		mdc = new ManageServerConnections();
	}
	
	@Override
    protected void onStart() {
        super.onStart();
        
        TextView label = (TextView) findViewById(R.id.veriLabel);
        label.setText("Enter Code for "+MainActivity.activeEmployee);
        
        Intent intent = getIntent();
        veriXML = intent.getStringExtra(ConfirmationActivity.activityKey);
	}
	
	@Override
    protected void onResume() {
        super.onResume();
        // The activity has become visible (it is now "resumed").
    }
    @Override
    protected void onPause() {
        super.onPause();
      //time out the
        /*handler.postDelayed(new Runnable() {
        	@Override
        	public void run() {
        		finish();
      			}
        }, 120000 );*/
        // Another activity is taking focus (this activity is about to be "paused").
    }
    @Override
    protected void onStop() {
        super.onStop();
        // The activity is no longer visible (it is now "stopped")
    }
    @Override
    protected void onDestroy() {
        super.onDestroy();
        // The activity is about to be destroyed.
    }
	
	public void stateMachine(View view){
    	if(view.getId()==R.id.verify){
    		EditText codeField = (EditText) findViewById(R.id.codeField);
    		String verificationCode = codeField.getText().toString();
    		codeField.setText("");
    		if(verificationCode.length()==6){
    			setContentView(R.layout.progress_bar);
    			veriXML = veriXML+"<code>"+verificationCode+"</code>";
    			veriXML = veriXML+"</pbtRequest>";
    			mdc.setUpdateVariable(veriXML);
    			new DownloadXMLTask().execute(MainActivity.url);
    		}
    		else{
    			Toast.makeText(VerificationActivity.this,"Code must be 6 digits!", Toast.LENGTH_LONG).show();
    		}
    	}
    	else{
    		finish();
    	}
    }
	
	public class DownloadXMLTask extends AsyncTask<String, Void, String> {
		private String status = "";
		private InputStream stream = null;
		private static final String DEBUG_TAG = "DownloadXMLTask";

		@Override
		protected String doInBackground(String... url) {
			try {
	    		stream = mdc.downloadUrl(url[0]);
	    		try	{
	    			status = XMLParser.parseXML(stream);
	    		}
	    		catch(Exception ex){
	    			ex.printStackTrace();
	    		}
	    	}
	    	catch(IOException ex){
	        	ex.printStackTrace();
	        }
	        finally {
	            if (stream != null) {
	            	try{
	            		stream.close();
	                }catch(IOException ex){
	                	
	                }
	            }
	        }
	    	return status;
		}
		
		@Override
	    protected void onPostExecute(String result) {
	    	if(result!=null){
	    		Log.d(DEBUG_TAG, result);
	    		if(result.equals("SUCCESSFUL")&&XMLParser.method.equals("faircap_verification")){
	    			ConfirmationActivity.updateStateTable(EnrollmentActivity2.STATE_COMPLETE);
	    			createResponseDialog(result,R.layout.success_prompt,R.id.success);
	    		}
	    		else{
	    			if(ManageServerConnections.timeout==null){
	    				createResponseDialog(result,R.layout.error_prompt,R.id.error);
	    			}
	    			else{
	    				createResponseDialog(ManageServerConnections.timeout,R.layout.error_prompt,R.id.error);
	    			}
	    		}
	    	}
	    }
	}
	
	private void createResponseDialog(String msg,int layout,int textView){
    	LayoutInflater li = LayoutInflater.from(this);
		View promptsView = li.inflate(layout, null);
		AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);
		alertDialogBuilder.setView(promptsView);
		
		final TextView successTextView = (TextView) promptsView.findViewById(textView);
		successTextView.setText(msg);
		
		// set dialog message
		alertDialogBuilder.setCancelable(false).setPositiveButton("OK",new DialogInterface.OnClickListener() {
			    public void onClick(DialogInterface dialog,int id) {
			    	//Intent employerIntent = new Intent(this,EmployerActivity.class);
			    	//startActivity(employerIntent);
			    	finish();
			    }
			  });
		
		AlertDialog alertDialog = alertDialogBuilder.create();
		alertDialog.show();
    }
	
	//xmlStr.append("<code>").append(code).append("</code>");
    //xmlStr.append("</pbtRequest>");
}
