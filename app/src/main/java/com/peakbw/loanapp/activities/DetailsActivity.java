package com.peakbw.loanapp.activities;

import java.util.ArrayList;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.MenuItem;
import android.widget.TextView;
import com.peakbw.loanapp.R;
import com.peakbw.loanapp.net.ManageServerConnections;

public class DetailsActivity extends Activity {
	
	private String employeeData;
	
	private String id,consent,photo,employer,employee,gender,dob,phone;
	private String limit;
	private String salary;
	private String employeeId;
	
	private TextView employerv,employeev,dobv,genderv,consentScannedv,idScannedv,photoCapturedv
				,borrowingLimit,netSalary,phonev;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_details);
		// Show the Up button in the action bar.
		setupActionBar();
		
		employerv = (TextView) findViewById(R.id.employer);
        employeev = (TextView) findViewById(R.id.cemployee);
        dobv = (TextView) findViewById(R.id.dobirth);
        genderv = (TextView) findViewById(R.id.gender);
        consentScannedv = (TextView) findViewById(R.id.consent);
        idScannedv = (TextView) findViewById(R.id.cid);
        photoCapturedv = (TextView) findViewById(R.id.photo);
        borrowingLimit = (TextView) findViewById(R.id.bLimit);
        netSalary = (TextView) findViewById(R.id.eSalary);
        phonev = (TextView) findViewById(R.id.phone);
	}

	/**
	 * Set up the {@link android.app.ActionBar}.
	 */
	private void setupActionBar() {

		getActionBar().setDisplayHomeAsUpEnabled(true);

	}
	
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		switch (item.getItemId()) {
		case android.R.id.home:
			finish();
			return true;
    	}
		
		return super.onOptionsItemSelected(item);
	}
	
	@Override
    protected void onStart() {
        super.onStart();
        
        Intent intent = getIntent();
		employeeData = intent.getStringExtra(HistoryActivity.activityKey); 
        setEmployeeFields(employeeData);
        
        employerv.setText(employer.toUpperCase());
        employeev.setText(employee+"-"+employeeId);
        dobv.setText(dob);
        genderv.setText(gender);
        phonev.setText(phone);
        borrowingLimit.setText(limit);
        netSalary.setText(salary);
        idScannedv.setText(id);
        consentScannedv.setText(consent);
        photoCapturedv.setText(photo);
    }
	
	@Override
    protected void onResume() {
        super.onResume();
        // The activity has become visible (it is now "resumed").
    }
	
    @Override
    protected void onPause() {
        super.onPause();
    }
    
    @Override
    protected void onStop() {
        super.onStop();
        // The activity is no longer visible (it is now "stopped")
    }
    @Override
    protected void onDestroy() {
        super.onDestroy();
        // The activity is about to be destroyed.
    }
    
    private void setEmployeeFields(String info){
		ArrayList<String> emppDetails = ManageServerConnections.split(info, "*");
		this.employee = emppDetails.get(1);
		this.employer = emppDetails.get(0);
		this.employeeId = emppDetails.get(8);
		this.dob = emppDetails.get(2);
		this.gender = emppDetails.get(3);
		this.phone = emppDetails.get(4);
		this.salary = emppDetails.get(6);
		this.limit = emppDetails.get(5);
		this.id = emppDetails.get(12);
		this.consent = emppDetails.get(13);
		this.photo = emppDetails.get(14);
	}

}
