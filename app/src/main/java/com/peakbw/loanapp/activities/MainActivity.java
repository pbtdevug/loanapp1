package com.peakbw.loanapp.activities;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.ContentValues;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.PackageManager.NameNotFoundException;
import android.content.pm.ResolveInfo;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.os.ResultReceiver;
import android.telephony.TelephonyManager;
import android.text.TextUtils;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;
import com.peakbw.loanapp.R;
import com.peakbw.loanapp.db.ReaderContract.Signature;
import com.peakbw.loanapp.db.ReaderDBHelper;
import com.peakbw.loanapp.net.ManageServerConnections;
import com.peakbw.loanapp.net.XMLParser;
import com.peakbw.loanapp.services.DownloadService;
import com.peakbw.loanapp.services.FTPService;

public class MainActivity extends Activity {
	
	//private Handler handler = new Handler(Looper.getMainLooper());
	private static final String DEBUG_TAG = "MainActivity";
	public static boolean wifiConnected = false;
    public static boolean mobileConnected = false;
    public static final int DIALOG_DOWNLOAD_PROGRESS = 0;
    public static final int DIALOG_DOWNLOAD_PROGRESS1 = 2;
    public static final int DIALOG_ALERT = 1;
    public static final int DIALOG_URL = 3;
    private ProgressDialog mProgressDialog;
    public static ProgressDialog dProgressDialog;
    private AlertDialog.Builder builder,urlDialog;
    private  ManageServerConnections mdc;
    private String upgradeUrl,latestVersion,signature;
    public static String phoneID,simSerialNumber/*,simNumber*/,activeEmployee = "";
    public static String date;
    public static ReaderDBHelper rDbHelper;
    private SQLiteDatabase db;
    public static String pin,versionName;
    public static  int lenghtOfFile,versionNumber;
    private ContentValues values;
    private EditText pinField;
    //public static final String url = "http://10.0.2.2:8084/MarcsWeb/Update";
    //public static final String url = = "http://192.168.1.99/FairCapital/faircapital_serverbkend/faircapital.test.php";
    public static String url;

	@Override
	protected void onCreate(Bundle savedInstanceState){
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);

        pinField = (EditText) findViewById(R.id.user_pini);
        pinField.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView textView, int id,KeyEvent keyEvent) {
                if (id == R.id.login || id == EditorInfo.IME_NULL) {
                    validate();
                    return true;
                }
                return false;
            }
        });
		
		//get phone unique identifier 
        TelephonyManager telephonyManager = (TelephonyManager)getSystemService(Context.TELEPHONY_SERVICE);
        phoneID = telephonyManager.getDeviceId();
        
        TelephonyManager telemamanger = (TelephonyManager) getSystemService(Context.TELEPHONY_SERVICE);
        simSerialNumber = telemamanger.getSimSerialNumber();
        simSerialNumber = simSerialNumber.substring(0,simSerialNumber.length()-1);
        
        //get app version info
        PackageInfo pinfo;
		try {
			pinfo = getPackageManager().getPackageInfo(getPackageName(), 0);
			versionNumber = pinfo.versionCode;
	        versionName = pinfo.versionName;
		} 
		catch (NameNotFoundException e) {
			e.printStackTrace();
		}
        
        date = getDate();
        mdc = new ManageServerConnections();

        values = new ContentValues();

        //create or open sqlite database
        rDbHelper = new ReaderDBHelper(getBaseContext());

        db = rDbHelper.getWritableDatabase();

        Intent serviceIntent = new Intent(this, FTPService.class);
        startService(serviceIntent);
	}
	
	@Override
	public void onStart(){
		super.onStart();

        updateConnectedFlags();

        // get updateSignature and connection url
        Cursor cursor = rDbHelper.getSignature();
        cursor.moveToFirst();
        signature = cursor.getString(cursor.getColumnIndexOrThrow(Signature.SIGNATURE));
        url = cursor.getString(cursor.getColumnIndexOrThrow(Signature.UPDATE_URL));

        if(url.equals("url")){
            showDialog(DIALOG_URL);
        }
	}
	
	public void login(View view){
		validate();
	}
	
	public void exit(View view){
		finish();
	}
	
	private void updateConnectedFlags() {
        ConnectivityManager connMgr = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);

        NetworkInfo activeInfo = connMgr.getActiveNetworkInfo();
        if (activeInfo != null && activeInfo.isConnected()) {
            wifiConnected = activeInfo.getType() == ConnectivityManager.TYPE_WIFI;
            mobileConnected = activeInfo.getType() == ConnectivityManager.TYPE_MOBILE;
        } else {
            wifiConnected = false;
            mobileConnected = false;
        }
        Log.d(DEBUG_TAG, "connected to wifi = "+wifiConnected);
        Log.d(DEBUG_TAG, "connected to mobile = "+mobileConnected);
    }
	
	public class DownloadXMLTask extends AsyncTask<String, String, String> {
		private String status = "";
		private InputStream stream = null;
		private static final String DEBUG_TAG = "DownloadXMLTask";
		
		ByteArrayOutputStream baos = new ByteArrayOutputStream();
		
		@SuppressWarnings("deprecation")
		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			showDialog(DIALOG_DOWNLOAD_PROGRESS);
		}

		@Override
		protected String doInBackground(String... url) {
			try {
	    		stream = mdc.downloadUrl(url[0]);
	    		if(stream!=null){
	    		try	{
	    			
	    			byte[] buffer = new byte[1024];
	                int len;
	                long total = 0;
	                
	                while ((len = stream.read(buffer)) > -1) {
	                	total += len;
	                	// After this onProgressUpdate will be called
	                	
	                    publishProgress(""+(int)((total*100)/lenghtOfFile));
	                    Log.d("ANDRO_ASYNC","length ="+lenghtOfFile);
	                    baos.write(buffer, 0, len);
	                }
	                baos.flush();
	                baos.close();
	                stream.close();
	    			
	    			status = XMLParser.parseXML(new ByteArrayInputStream(baos.toByteArray()));
	    		}
	    		catch(Exception ex){
	    			ex.printStackTrace();
	    		}
	    		}
	    	}
	    	catch(IOException ex){
	        	ex.printStackTrace();
	        }
	        finally {
	            if (stream != null) {
	            	try{
	            		stream.close();
	                }catch(IOException ex){
	                	
	                }
	            }
	        }
	    	return status;
		}
		
		protected void onProgressUpdate(String... progress) {
			 Log.d("ANDRO_ASYNC",progress[0]);
			 mProgressDialog.setProgress(Integer.parseInt(progress[0]));
		}

		@SuppressWarnings("deprecation")
		@Override
	    protected void onPostExecute(String result) {
			dismissDialog(DIALOG_DOWNLOAD_PROGRESS);
	    	if(result!=null){
	    		Log.d(DEBUG_TAG, "result = "+result);
	    		//check for latest version
	    		upgradeUrl = XMLParser.upgradeURL;
	    		latestVersion = XMLParser.version;
	    		if(upgradeUrl!=null&&latestVersion!=null){
	    			float currentVersion = Float.parseFloat(versionName);
	    			float newVersion = Float.parseFloat(latestVersion);
	    			
	    			if(newVersion>currentVersion){
	    				notifyUser(upgradeUrl);
	    			}
	    			else{
	    				//start EmployerActivity
			    		startEmployerActivity();
	    			}
	    		}
	    		else{
	    			//start EmployerActivity
		    		startEmployerActivity();
	    		}
	    	}
	    }
	}
	
	private String createUpdateXML(String pin,String signature){
		StringBuffer xmlStr = new StringBuffer();
        xmlStr.append("<?xml version='1.0' encoding='UTF-8'?>");
        xmlStr.append("<pbtRequest>");
        xmlStr.append("<pin>").append(pin).append("</pin>");
        xmlStr.append("<method>").append("faircap_update").append("</method>");
        xmlStr.append("<imei>").append(phoneID).append("</imei>");
        xmlStr.append("<msisdn>").append(simSerialNumber).append("</msisdn>");
        xmlStr.append("<signature>").append(signature).append("</signature>");
        xmlStr.append("<appVersion>").append(versionName).append("</appVersion>");
        xmlStr.append("</pbtRequest>");
        System.out.println(xmlStr.toString());
        return xmlStr.toString();
    }
	
	private String getDate() {//Fri 29 Nov 2013
        ArrayList<String> dateInfo = ManageServerConnections.split((new Date()).toString()," ");
        String txn = dateInfo.get(2).toString()+" "+dateInfo.get(1)+" "+dateInfo.get(5).toString();
        return txn;
    }
	
	private void startEmployerActivity(){
		Intent employerIntent = new Intent(this,EmployerActivity.class);
		startActivity(employerIntent);
		finish();
	}
	
	@Override
    protected void onResume() {
        super.onResume();
        // The activity has become visible (it is now "resumed").
    }
    @Override
    protected void onPause() {
        super.onPause();
      //time out the
        /*handler.postDelayed(new Runnable() {
        	@Override
        	public void run() {
        		finish();
      			}
        }, 120000 );*/
        // Another activity is taking focus (this activity is about to be "paused").
    }
    @Override
    protected void onStop() {
        super.onStop();
        // The activity is no longer visible (it is now "stopped")
    }
    @Override
    protected void onDestroy() {
        super.onDestroy();
        // The activity is about to be destroyed.
    }
    
    @Override
    protected Dialog onCreateDialog(int id) {
        switch (id) {
			case DIALOG_DOWNLOAD_PROGRESS:
				mProgressDialog = new ProgressDialog(this);
				mProgressDialog.setMessage("please wait");
				mProgressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
				mProgressDialog.setCancelable(true);
				mProgressDialog.show();
				
				return mProgressDialog;
				
			case DIALOG_ALERT:
				builder = new AlertDialog.Builder(this);
				builder.setTitle("Upgrade M-Payday");
				builder.setMessage("Do you want to upgrade M-Payday version '"+versionName+"' to version '"+latestVersion+"?");
				builder.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
					// do something when the button is clicked
					public void onClick(DialogInterface dialog, int id) {
			            //start download service
						Log.d(DEBUG_TAG, "yes selected");
						startDowloadService();
					}
				});
				builder.setNegativeButton("No", new DialogInterface.OnClickListener() {
					@Override
					public void onClick(DialogInterface dialog, int id) {
						//start EmployerActivity
			    		startEmployerActivity();
					}
				});
				return builder.create();
				
			case DIALOG_DOWNLOAD_PROGRESS1:
				dProgressDialog = new ProgressDialog(this);
				dProgressDialog.setMessage("dowloading apk..");
				dProgressDialog.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
				dProgressDialog.setCancelable(true);
				
				return dProgressDialog;

            case DIALOG_URL:
                urlDialog = new AlertDialog.Builder(this);
                LayoutInflater li = LayoutInflater.from(this);
                View promptsView = li.inflate(R.layout.url_prompot, null);
                final EditText urlInput = (EditText) promptsView.findViewById(R.id.urlField);
                urlInput.setText("http://www.faircapitalfs.com/enrol/faircapital.test.php");
                urlDialog.setView(promptsView);
                urlDialog.setCancelable(false);
                urlDialog.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    // do something when the button is clicked
                    public void onClick(DialogInterface dialog, int id) {
                        String urlString = urlInput.getText().toString();
                        if(urlString.length()>=20){
                            url = urlString;
                            //update table signature
                            values.put(Signature.UPDATE_URL, url);
                            //excute query
                            int count = db.update(Signature.TABLE_NAME,values,null,null);
                            //
                            Log.d(DEBUG_TAG, "New URL= : "+count+" "+url);
                            values.clear();
                        }
                        else{
                            Toast.makeText(MainActivity.this,"Invalid URL!", Toast.LENGTH_LONG).show();
                        }
                    }
                });
                urlDialog.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int id) {
                    }
                });
                return urlDialog.create();
				
			default:
				return null;
        }
    }
    
    @SuppressWarnings("deprecation")
	private void notifyUser(String upgradeUrl){
    	Log.d(DEBUG_TAG, "alert user");
    	showDialog(DIALOG_ALERT);
    }
    
    /**
	 * @param context used to check the device version and DownloadManager information
	 * @return true if the download manager is available
	 */
	public static boolean isDownloadManagerAvailable(Context context) {
	    try {
	        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.GINGERBREAD) {
	            return false;
	        }
	        Intent intent = new Intent(Intent.ACTION_MAIN);
	        intent.addCategory(Intent.CATEGORY_LAUNCHER);
	        intent.setClassName("com.android.providers.downloads.ui", "com.android.providers.downloads.ui.DownloadList");
	        List<ResolveInfo> list = context.getPackageManager().queryIntentActivities(intent,
	                PackageManager.MATCH_DEFAULT_ONLY);
	        return list.size() > 0;
	    } catch (Exception e) {
	        return false;
	    }
	}
	
	@SuppressWarnings("deprecation")
	private void startDowloadService(){
		Log.d(DEBUG_TAG, "In startDownloadService");
		showDialog(DIALOG_DOWNLOAD_PROGRESS1);
		//dProgressDialog.show();
		/*if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
			Log.d(DEBUG_TAG, "version > "+Build.VERSION_CODES.HONEYCOMB);
			DownloadManager.Request request = new DownloadManager.Request(Uri.parse(upgradeUrl));
			request.setDescription("downloading new application");
			request.setTitle("Download APK");
			
			request.allowScanningByMediaScanner();
		    request.setNotificationVisibility(DownloadManager.Request.VISIBILITY_VISIBLE_NOTIFY_COMPLETED);
		    
		    request.setDestinationInExternalPublicDir(Environment.DIRECTORY_DOWNLOADS, "upgrade.apk");

		    // get download service and enqueue file
		    DownloadManager manager = (DownloadManager) getSystemService(Context.DOWNLOAD_SERVICE);
		    manager.enqueue(request);
		    Log.d(DEBUG_TAG, "done downloading file");
		}
		else{*/
			Intent downloadIntent = new Intent(this, DownloadService.class);
			downloadIntent.putExtra("url", upgradeUrl);
			downloadIntent.putExtra("receiver", new DownloadReceiver(new Handler()));
			//downloadIntent.setData(Uri.parse(upgradeUrl));
			startService(downloadIntent);
			Log.d(DEBUG_TAG, "started download service...");
		//}
	}
	
	public class DownloadReceiver extends ResultReceiver{
	    public DownloadReceiver(Handler handler) {
	        super(handler);
	    }

	    @Override
	    protected void onReceiveResult(int resultCode, Bundle resultData) {
	        super.onReceiveResult(resultCode, resultData);
	        Log.d(DEBUG_TAG, "Received download");
	        if (resultCode == DownloadService.UPDATE_PROGRESS) {
	            int progress = resultData.getInt("progress");
	            dProgressDialog.setProgress(progress);
	            if (progress == 100) {
	            	dProgressDialog.dismiss();
	            	installUpdate();
	            }
	        }
	    }
	}
	
	private void installUpdate(){
		Log.d(DEBUG_TAG, "started Installation");
		String filePath = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS)+ "/upgrade.apk";
		
		Intent intent = new Intent(Intent.ACTION_VIEW);
        intent.setDataAndType(Uri.fromFile(new File(filePath)), "application/vnd.android.package-archive");
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK); // without this flag android returned a intent error!
        startActivity(intent);
		
		
		/*Process installProcess = null;
		int installResult = -1337;

		try {
			Log.d(DEBUG_TAG, "file Path = "+filePath);
		    installProcess = Runtime.getRuntime().exec("su -c pm install -r " + filePath);
		} 
		catch (IOException e) {
			e.printStackTrace();
		    // Handle IOException the way you like.
		}

		if (installProcess != null) {
		    try {
		        installResult = installProcess.waitFor();
		    } 
		    catch(InterruptedException e) {
		    	e.printStackTrace();
		        // Handle InterruptedException the way you like.
		    }

		    if (installResult == 0) {
		    	Log.d(DEBUG_TAG, "Installation complete");
		        // Success!
		    } 
		    else {
		    	Log.d(DEBUG_TAG, "Installation failed");
		        // Failure. :-/
		    }
		} 
		else {
			Log.d(DEBUG_TAG, "can not Install");
		    // Failure 2. :-(
		}*/
	}

    private void validate(){
        pin = pinField.getText().toString();
        pinField.setText("");

        Log.d(DEBUG_TAG, "Pin = "+pin);

        boolean cancel = false;
        View focusView = null;

        if (TextUtils.isEmpty(pin)) {
            pinField.setError("PIN Required!");
            focusView = pinField;
            cancel = true;
        } else if (pin.length() < 4) {
            pinField.setError("PIN must be 4 digits!");
            focusView = pinField;
            cancel = true;
        }

        if (cancel) {
            // There was an error; don't attempt continue and focus the first
            // form field with an error.
            focusView.requestFocus();
        } else {
            String request = createUpdateXML(pin, signature);
            mdc.setUpdateVariable(request);
            new DownloadXMLTask().execute(url);
        }
    }
	
}
