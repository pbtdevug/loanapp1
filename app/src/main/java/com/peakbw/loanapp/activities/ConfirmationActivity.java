package com.peakbw.loanapp.activities;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.ContentValues;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.sqlite.SQLiteDatabase;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;
import com.peakbw.loanapp.R;
import com.peakbw.loanapp.db.ReaderContract.State;
import com.peakbw.loanapp.db.ReaderDBHelper;
import com.peakbw.loanapp.net.ManageServerConnections;
import com.peakbw.loanapp.net.XMLParser;

public class ConfirmationActivity extends Activity {

    private static final String DEBUG_TAG = "DownloadXMLTask";
    public static final String activityKey = "com.peakbw.loanapp.activities.ConfirmationActivity";
    private String employeeDAta;
    private String id,consent,photo,employer,employee,gender,dob;
    private static String phone;
    private  ManageServerConnections mdc;
    private String limit;
    private String salary;
    private String employeeId;
    private String employerId;
    private String idScanned;
    private String consentScanned,hiringDate;
    public static ProgressDialog progressDialog;
    private AlertDialog.Builder vericationDialog;
    public static final int DIALOG_REQUEST_PROGRESS = 0;
    public static final int DIALOG_VERIFICATION = 1;
    private String photoCaptured;
    private String veriXML;
    private static ReaderDBHelper rDbHelper;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_confirmation);
        // Show the Up button in the action bar.

        rDbHelper = new ReaderDBHelper(getBaseContext());

        setupActionBar();
        setTitle("Confirm Details");
        mdc = new ManageServerConnections();
    }

    /**
     * Set up the {@link android.app.ActionBar}.
     */
    private void setupActionBar() {

        getActionBar().setDisplayHomeAsUpEnabled(true);

    }

    @SuppressLint("DefaultLocale")
    @Override
    protected void onStart() {
        super.onStart();
        // The activity is about to become visible.

        Intent intent = getIntent();

        if(intent.hasExtra(EnrollmentActivity2.activityKey)){
            employeeDAta = intent.getStringExtra(EnrollmentActivity2.activityKey);
            setEmployeeFields(employeeDAta);
        }

        TextView employerv = (TextView) findViewById(R.id.employer);
        TextView employeev = (TextView) findViewById(R.id.cemployee);
        TextView dobv = (TextView) findViewById(R.id.dobirth);
        TextView genderv = (TextView) findViewById(R.id.gender);
        TextView consentScannedv = (TextView) findViewById(R.id.consent);
        TextView idScannedv = (TextView) findViewById(R.id.cid);
        TextView photoCapturedv = (TextView) findViewById(R.id.photo);
        TextView borrowingLimit = (TextView) findViewById(R.id.bLimit);
        TextView netSalary = (TextView) findViewById(R.id.eSalary);
        TextView phonev = (TextView) findViewById(R.id.phone);

        employerv.setText(employer.toUpperCase());
        employeev.setText(employee);
        dobv.setText(dob);
        genderv.setText(gender);
        phonev.setText(phone);

        consentScannedv.setText(consentScanned.toUpperCase());
        idScannedv.setText(idScanned.toUpperCase());
        photoCapturedv.setText(photoCaptured.toUpperCase());
        borrowingLimit.setText(limit);
        netSalary.setText(salary);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        switch (item.getItemId()) {
            case android.R.id.home:
                Log.d(DEBUG_TAG, "Up button clicked!");
                finish();
                return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onResume() {
        super.onResume();
        // The activity has become visible (it is now "resumed").
    }
    @Override
    protected void onPause() {
        super.onPause();
        //time out the
        /*handler.postDelayed(new Runnable() {
        	@Override
        	public void run() {
        		finish();
      			}
        }, 120000 );*/
        // Another activity is taking focus (this activity is about to be "paused").
    }
    @Override
    protected void onStop() {
        super.onStop();
        // The activity is no longer visible (it is now "stopped")
    }
    @Override
    protected void onDestroy() {
        super.onDestroy();
        // The activity is about to be destroyed.
    }

    public void confirmationStateMachine(View view){
        if(view.getId()==R.id.ccancel){
            EnrollmentActivity2.getInstance().finish();
            EnrollmentActivity1.getInstance().finish();
            finish();
        }
        else{
            if(consentScanned.equals("true")){
                if(idScanned.equals("true")){
                    if(photoCaptured.equals("true")){
                        updateStateTable(EnrollmentActivity2.STATE_ENROLLMENT);
                        String xmlRequest = createEnrollmentXML();
                        mdc.setUpdateVariable(xmlRequest);
                        new DownloadXMLTask().execute(MainActivity.url);
                    }
                    else{
                        createAlertDialog("Photograph not captured.\nDo you want to save Enrollment as pending!",R.layout.confirmation_prompt,R.id.message);
                    }
                }
                else{
                    createAlertDialog("ID not Scanned.\nDo you want to save Enrollment as pending!",R.layout.confirmation_prompt,R.id.message);
                }
            }
            else{
                createAlertDialog("Consent not Scanned.\nDo you want to save Enrollment as pending!",R.layout.confirmation_prompt,R.id.message);
            }
        }

    }

    public class DownloadXMLTask extends AsyncTask<String, Void, String> {
        private String status = "";
        private InputStream stream = null;
        private static final String DEBUG_TAG = "DownloadXMLTask";

        @SuppressWarnings("deprecation")
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            showDialog(DIALOG_REQUEST_PROGRESS);
        }

        @Override
        protected String doInBackground(String... url) {
            try {
                stream = mdc.downloadUrl(url[0]);
                try	{
                    status = XMLParser.parseXML(stream);
                }
                catch(Exception ex){
                    ex.printStackTrace();
                }
            }
            catch(IOException ex){
                ex.printStackTrace();
            }
            finally {
                if (stream != null) {
                    try{
                        stream.close();
                    }catch(IOException ex){

                    }
                }
            }
            return status;
        }

        @SuppressWarnings("deprecation")
        @Override
        protected void onPostExecute(String result) {
            dismissDialog(DIALOG_REQUEST_PROGRESS);
            if(result!=null){
                Log.d(DEBUG_TAG, result);
                if(result.equals("SUCCESSFUL")&&XMLParser.method.equals("faircap_enrollment")){
                    updateStateTable(EnrollmentActivity2.STATE_VERIFICATION);
                    startVerification();
                }
                else if(result.equals("SUCCESSFUL")&&XMLParser.method.equals("faircap_verification")){
                    updateStateTable(EnrollmentActivity2.STATE_COMPLETE);
                    createResponseDialog(result,R.layout.success_prompt,R.id.success);
                }
                else if(result.equalsIgnoreCase("REGISTERED")&&XMLParser.method.equals("faircap_enrollment")){
                    updateStateTable(EnrollmentActivity2.STATE_VERIFICATION);
                    startVerification();
                    ///
                }
                else{
                    if(ManageServerConnections.timeout==null){
                        if(!result.equals("")&&XMLParser.method.equals("faircap_enrollment")){
                            updateStateTable(EnrollmentActivity2.STATE_VERIFICATION);
                            createResponseDialog(result,R.layout.error_prompt,R.id.error);
                        }
                        else if(!result.equals("")){
                            //deleteRow(phone);
                            createResponseDialog(result,R.layout.error_prompt,R.id.error);
                        }
                        else{
                            //deleteRow(phone);
                            createResponseDialog("Connection Error",R.layout.error_prompt,R.id.error);
                        }
                    }
                    else{
                        //deleteRow(phone);
                        createResponseDialog(ManageServerConnections.timeout,R.layout.error_prompt,R.id.error);
                    }
                }
            }
            else{
                createResponseDialog("Invalid Response",R.layout.error_prompt,R.id.error);
            }
            //clear fields
            EnrollmentActivity1.dateField.setText("");
            EnrollmentActivity1.phoneField.setText("");
            EnrollmentActivity1.limitField.setText("");
            EnrollmentActivity1.salaryField.setText("");
        }
    }

    private void createResponseDialog(String msg,int layout,int textView){
        LayoutInflater li = LayoutInflater.from(this);
        View promptsView = li.inflate(layout, null);
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);
        alertDialogBuilder.setView(promptsView);

        final TextView successTextView = (TextView) promptsView.findViewById(textView);
        successTextView.setText(msg);

        // set dialog message
        alertDialogBuilder.setCancelable(false).setPositiveButton("OK",new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog,int id) {
                //finish previous activities
                EnrollmentActivity2.getInstance().finish();
                EnrollmentActivity1.getInstance().finish();

                finish();
            }
        });

        AlertDialog alertDialog = alertDialogBuilder.create();
        alertDialog.show();
    }

    public String createEnrollmentXML(){
        StringBuffer xmlStr = new StringBuffer();
        xmlStr.append("<?xml version='1.0' encoding='UTF-8'?>");
        xmlStr.append("<pbtRequest>");
        xmlStr.append("<method>").append("faircap_enrollment").append("</method>");
        xmlStr.append("<pin>").append(MainActivity.pin).append("</pin>");
        xmlStr.append("<imei>").append(MainActivity.phoneID).append("</imei>");
        xmlStr.append("<iccid>").append(MainActivity.simSerialNumber).append("</iccid>");
        xmlStr.append("<trxnNumber>").append(ManageServerConnections.getTrxnNumber()).append("</trxnNumber>");
        xmlStr.append("<employerName>").append(employer).append("</employerName>");
        xmlStr.append("<employerID>").append(employerId).append("</employerID>");
        xmlStr.append("<employeeName>").append(employee).append("</employeeName>");
        xmlStr.append("<employeeID>").append(employeeId).append("</employeeID>");
        xmlStr.append("<phoneNumber>").append(phone).append("</phoneNumber>");
        xmlStr.append("<gender>").append(gender).append("</gender>");
        xmlStr.append("<dateOfBirth>").append(dob).append("</dateOfBirth>");
        xmlStr.append("<appointmentDate>").append(hiringDate).append("</appointmentDate>");
        xmlStr.append("<idScan>").append(id).append("</idScan>");
        xmlStr.append("<consentScan>").append(consent).append("</consentScan>");
        xmlStr.append("<photo>").append(photo).append("</photo>");
        xmlStr.append("<borrowingLimit>").append(limit).append("</borrowingLimit>");
        xmlStr.append("<netSalary>").append(salary).append("</netSalary>");
        xmlStr.append("</pbtRequest>");

        MainActivity.activeEmployee = employee+"-"+employeeId;

        System.out.println(xmlStr.toString());
        return xmlStr.toString();
    }

    private String createVerificationXML(){
        StringBuffer xmlStr = new StringBuffer();
        xmlStr.append("<?xml version='1.0' encoding='UTF-8'?>");
        xmlStr.append("<pbtRequest>");
        xmlStr.append("<method>").append("faircap_verification").append("</method>");
        xmlStr.append("<pin>").append(MainActivity.pin).append("</pin>");
        xmlStr.append("<imei>").append(MainActivity.phoneID).append("</imei>");
        xmlStr.append("<iccid>").append(MainActivity.simSerialNumber).append("</iccid>");
        xmlStr.append("<employeeID>").append(employeeId).append("</employeeID>");
        xmlStr.append("<employerID>").append(employerId).append("</employerID>");
        System.out.println(xmlStr.toString());
        return xmlStr.toString();
    }

    public static int updateStateTable(String state){
        SQLiteDatabase db = rDbHelper.getReadableDatabase();

        // New value for one column
        ContentValues values = new ContentValues();
        values.put(State.STATUS, state);

        // Which row to update, based on the ID
        String selection = State.PHONE_NUMBER + " = ?";
        String[] selectionArgs = {phone};

        int count = db.update(State.TABLE_NAME,values,selection,selectionArgs);

        values.clear();
        Log.d(DEBUG_TAG, "Affected Row ID = "+count);
        return count;
    }

    private void setEmployeeFields(String info){
        ArrayList<String> emppDetails = ManageServerConnections.split(info, "*");
        this.employee = emppDetails.get(1);
        this.employer = emppDetails.get(0);
        this.employerId = emppDetails.get(7);
        this.employeeId = emppDetails.get(8);
        this.hiringDate = emppDetails.get(9);
        this.dob = emppDetails.get(2);
        this.gender = emppDetails.get(3);
        ConfirmationActivity.phone = emppDetails.get(4);
        this.salary = emppDetails.get(6);
        this.limit = emppDetails.get(5);
        this.idScanned = emppDetails.get(10);
        this.consentScanned = emppDetails.get(11);
        this.photoCaptured = emppDetails.get(12);
        this.id = emppDetails.get(13);
        this.consent = emppDetails.get(14);
        this.photo = emppDetails.get(15);
    }

    private void createAlertDialog(String msg,int layout,int textView){
        LayoutInflater li = LayoutInflater.from(this);
        View promptsView = li.inflate(layout, null);
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);
        alertDialogBuilder.setView(promptsView);

        final TextView alertTextView = (TextView) promptsView.findViewById(textView);
        alertTextView.setText(msg);

        // set dialog message
        alertDialogBuilder.setCancelable(false).setPositiveButton("OK",new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog,int id) {
                updateStateTable(EnrollmentActivity2.STATE_DOCUMENTS);
                EnrollmentActivity2.getInstance().finish();
                EnrollmentActivity1.getInstance().finish();
                finish();
            }
        }).setNegativeButton("Cancel",new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog,int id) {
                dialog.cancel();
            }
        });

        AlertDialog alertDialog = alertDialogBuilder.create();
        alertDialog.show();
    }

    @SuppressWarnings("deprecation")
    private void startVerification(){
        veriXML = createVerificationXML();
        showDialog(DIALOG_VERIFICATION);
        //Intent veriIntent = new Intent(this,VerificationActivity.class);
        //veriIntent.putExtra(activityKey, createVerificationXML());
        //finish previous activities
        //EnrollmentActivity2.getInstance().finish();
        //EnrollmentActivity1.getInstance().finish();
        //
        //startActivity(veriIntent);
        //finish();
    }

    private void deleteRow(String phoneNumber){
        SQLiteDatabase db = rDbHelper.getReadableDatabase();
        // Define 'where' part of query.
        String selection = State.PHONE_NUMBER + " = ?";
        // Specify arguments in placeholder order.
        String[] selectionArgs = { phoneNumber };
        // Issue SQL statement.
        db.delete(State.TABLE_NAME, selection, selectionArgs);
    }

    @Override
    protected Dialog onCreateDialog(int id) {
        switch (id) {
            case DIALOG_REQUEST_PROGRESS:
                progressDialog = new ProgressDialog(this);
                progressDialog.setMessage("please wait");
                progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
                progressDialog.setCancelable(true);
                progressDialog.show();

                return progressDialog;

            case DIALOG_VERIFICATION:
                vericationDialog = new AlertDialog.Builder(this);
                LayoutInflater li = LayoutInflater.from(this);
                View promptsView = li.inflate(R.layout.verification_prompt, null);
                final EditText veriInput = (EditText) promptsView.findViewById(R.id.verification_code);
                vericationDialog.setView(promptsView);
                vericationDialog.setCancelable(false);
                vericationDialog.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    // do something when the button is clicked
                    public void onClick(DialogInterface dialog, int id) {
                        String veriCode = veriInput.getText().toString();
                        if(veriCode.length()==6){
                            veriXML = veriXML+"<code>"+veriCode+"</code>";
                            veriXML = veriXML+"</pbtRequest>";
                            mdc.setUpdateVariable(veriXML);
                            new DownloadXMLTask().execute(MainActivity.url);
                        }
                        else{
                            Toast.makeText(ConfirmationActivity.this,"Code must be 6 digits!", Toast.LENGTH_LONG).show();
                        }
                    }
                });
                vericationDialog.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int id) {
                    }
                });
                return vericationDialog.create();

            default:
                return null;
        }
    }
}
