package com.peakbw.loanapp.activities;

import java.util.ArrayList;
import java.util.Hashtable;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.ContentValues;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;
import com.peakbw.loanapp.R;
import com.peakbw.loanapp.db.ReaderContract;
import com.peakbw.loanapp.db.ReaderContract.Employee;
import com.peakbw.loanapp.db.ReaderContract.Employer;
import com.peakbw.loanapp.db.ReaderDBHelper;

public class EmployerActivity extends Activity implements OnItemSelectedListener{

	//private Handler handler = new Handler(Looper.getMainLooper());
	private Spinner employerSpinner;
	public static String employeeName,employeeId;
	private String employerName;
	private static final String DEBUG_TAG = "EmployerActivity";
	private Hashtable<String,String> employerHash;
    private ReaderDBHelper rDbHelper;
    private EditText idField;
	
	public static final String EMPLOYEE = "employee";
	public static final String EMPLOYEE_ID = "employe_id";
	public static final String EMPLOYER = "employer";
	public static final String EMPLOYER_ID = "employer_id";
	public static final String DOB = "dob";
	public static final String PHONE = "p_number";
	public static final String BORROWING_LIMIT = "limit";
	public static final String NET_SALARY = "salary";
	public static final String ID_FILE = "id";
	public static final String PHOTO_FILE = "photo";
	public static final String CONSENT_FILE = "consent";
	
	public static final String ID_SCANNED = "id_scanned";
	public static final String CONSENT_SCANNED = "consent_scanned";
	public static final String PHOTO_CAPTURED = "photo_captured";

    public static final int DIALOG_URL = 3;
    private AlertDialog.Builder urlDialog;

    private static SQLiteDatabase db;
    private ContentValues values;
	
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_employer);
		// Show the Up button in the action bar.
		setupActionBar();
		
		setTitle("Employer Details");

        idField = (EditText) findViewById(R.id.employeeId);
        idField.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView textView, int id,KeyEvent keyEvent) {
                if (id == R.id.login || id == EditorInfo.IME_NULL) {
                    validate();
                    return true;
                }
                return false;
            }
        });

        rDbHelper = new ReaderDBHelper(getBaseContext());
		
        //run query that returns all employers MainActivity.rDbHelper
        Cursor cursor = rDbHelper.getAllEmpoyers();
        
        ArrayList<String> employerList = new ArrayList<String>();
        
        employerHash = new Hashtable<String,String>();
        
        while(cursor.moveToNext()){
        	String employer = cursor.getString(cursor.getColumnIndexOrThrow(Employer.EMPLOYER_NAME));
        	String employerId = cursor.getString(cursor.getColumnIndexOrThrow(Employer.EMPLOYER_ID));
        	employerHash.put(employer, employerId);
        	employerList.add(employer);
        }
        
        employerSpinner = (Spinner) findViewById(R.id.employerSpinner);
        employerSpinner.setOnItemSelectedListener(this);
        
        ArrayAdapter<String> employersAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item,employerList);
        employersAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        employerSpinner.setAdapter(employersAdapter);

        //get db instance
        db = rDbHelper.getWritableDatabase();
        values = new ContentValues();
	}

	/**
	 * Set up the {@link android.app.ActionBar}.
	 */
	private void setupActionBar() {
		getActionBar().setDisplayHomeAsUpEnabled(true);
	}
	
	
	@Override
    protected void onStart() {
        super.onStart();
        // The activity is about to become visible.
    }
	
	@Override
    protected void onResume() {
        super.onResume();
        // The activity has become visible (it is now "resumed").
    }
    @Override
    protected void onPause() {
        super.onPause();
      //time out the
        /*handler.postDelayed(new Runnable() {
        	@Override
        	public void run() {
        		finish();
      			}
        }, 120000 );*/
        // Another activity is taking focus (this activity is about to be "paused").
    }
    @Override
    protected void onStop() {
        super.onStop();
        // The activity is no longer visible (it is now "stopped")
    }
    @Override
    protected void onDestroy() {
        super.onDestroy();
        // The activity is about to be destroyed.
    }

	@Override
	public void onItemSelected(AdapterView<?> parent, View view, int pos,long id) {
		if(parent.equals(employerSpinner)){
			employerName = (String)parent.getItemAtPosition(pos);
		}
		
	}

	@Override
	public void onNothingSelected(AdapterView<?> arg0) {
		// TODO Auto-generated method stub
		
	}
	
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.employer, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		switch (item.getItemId()) {
		
		case R.id.action_history:
			//Intent histIntent = new Intent(this, HistoryActivity.class);
	        //startActivity(histIntent);
            Intent histIntent = new Intent(this, HistoryCollection.class);
            startActivity(histIntent);
			return true;

        case R.id.action_url:
            showDialog(DIALOG_URL);
            return true;
		
		case android.R.id.home:
			finish();
			return true;

    }
		
		return super.onOptionsItemSelected(item);
	}
	
	public void cancel(View view){
		finish();
	}

	public void getEmployeDetails(View view){

        validate();

		/*String id = idField.getText().toString();
		idField.setText("");
		Log.d(DEBUG_TAG, "EmployeeID = "+id);
		Log.d(DEBUG_TAG, "EmployerName = "+employerName);
		if(id.length()>0){
			
			Cursor cursor = rDbHelper.getEmployeeName(id,employerHash.get(employerName));
			
			//int count = cursor.getCount();
			if(cursor != null&&cursor.moveToFirst()==true){
				String name = cursor.getString(cursor.getColumnIndexOrThrow(Employee.EMPLOYEE_NAME));
			
				employeeName = name;
				employeeId = id;
				startEnrollmentActivity1();
			}
			else{
				Toast.makeText(EmployerActivity.this, "Employee not currently in database!", Toast.LENGTH_SHORT).show();
			}
		}
		else{
			Toast.makeText(EmployerActivity.this, "enter Employee ID to continue!", Toast.LENGTH_SHORT).show();
		}*/
	}
	
	private void startEnrollmentActivity1(){
		Intent enrollIntent = new Intent(this,EnrollmentActivity1.class);
		enrollIntent.putExtra(EMPLOYEE,employeeName);
		enrollIntent.putExtra(EMPLOYEE_ID,employeeId);
		enrollIntent.putExtra(EMPLOYER,employerName);
		enrollIntent.putExtra(EMPLOYER_ID,employerHash.get(employerName));
		enrollIntent.putExtra(DOB,"");
		enrollIntent.putExtra(PHONE,"");
		enrollIntent.putExtra(BORROWING_LIMIT,"");
		enrollIntent.putExtra(NET_SALARY,"");
		
		enrollIntent.putExtra(EmployerActivity.ID_FILE,"0");
		enrollIntent.putExtra(EmployerActivity.CONSENT_FILE,"0");
		enrollIntent.putExtra(EmployerActivity.PHOTO_FILE,"0");
		
		enrollIntent.putExtra(EmployerActivity.ID_SCANNED,"false");
		enrollIntent.putExtra(EmployerActivity.CONSENT_SCANNED,"false");
		enrollIntent.putExtra(EmployerActivity.PHOTO_CAPTURED,"false");
		startActivity(enrollIntent);
	}

    @Override
    protected Dialog onCreateDialog(int id) {
        switch (id) {

            case DIALOG_URL:
                urlDialog = new AlertDialog.Builder(this);
                LayoutInflater li = LayoutInflater.from(this);
                View promptsView = li.inflate(R.layout.url_prompot, null);
                final EditText urlInput = (EditText) promptsView.findViewById(R.id.urlField);
                urlInput.setText(MainActivity.url);
                urlDialog.setView(promptsView);
                urlDialog.setCancelable(false);
                urlDialog.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    // do something when the button is clicked
                    public void onClick(DialogInterface dialog, int id) {
                        String urlString = urlInput.getText().toString();
                        if(urlString.length()>=20){
                            MainActivity.url = urlString;
                            //update table signature
                            values.put(ReaderContract.Signature.UPDATE_URL, urlString);
                            //excute query
                            int count = db.update(ReaderContract.Signature.TABLE_NAME,values,null,null);
                            //
                            Log.d(DEBUG_TAG, "New URL= : "+count+" "+urlString);
                            values.clear();
                        }
                        else{
                            Toast.makeText(EmployerActivity.this,"Invalid URL!", Toast.LENGTH_LONG).show();
                        }
                    }
                });
                urlDialog.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int id) {
                    }
                });
                return urlDialog.create();

            default:
                return null;
        }
    }

    private void validate(){
        String id = idField.getText().toString();
        idField.setText("");

        Log.d(DEBUG_TAG, "Employee ID = "+id);

        boolean cancel = false;
        View focusView = null;

        if (TextUtils.isEmpty(id)) {
            idField.setError("Staff ID Required!");
            focusView = idField;
            cancel = true;
        }else{
            Cursor cursor = rDbHelper.getEmployeeName(id,employerHash.get(employerName));

            //int count = cursor.getCount();
            if(cursor != null&&cursor.moveToFirst()==true){
                String name = cursor.getString(cursor.getColumnIndexOrThrow(Employee.EMPLOYEE_NAME));

                employeeName = name;
                employeeId = id;
                cancel = false;

            }else{
                idField.setError("Employee not in database!");
                focusView = idField;
                cancel = true;
            }
        }

        if (cancel) {
            // There was an error; don't attempt continue and focus the first
            // form field with an error.
            focusView.requestFocus();
        } else {
            startEnrollmentActivity1();
        }
    }

}
