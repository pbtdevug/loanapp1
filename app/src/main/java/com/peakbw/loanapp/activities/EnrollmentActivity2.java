package com.peakbw.loanapp.activities;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStreamWriter;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import android.app.Activity;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.hardware.Camera;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.util.Log;
import android.view.ContextMenu;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.Toast;
import com.peakbw.loanapp.R;
import com.peakbw.loanapp.camera.AlbumStorageDirFactory;
import com.peakbw.loanapp.camera.BaseAlbumDirFactory;
import com.peakbw.loanapp.camera.FroyoAlbumDirFactory;
import com.peakbw.loanapp.db.ReaderContract;
import com.peakbw.loanapp.db.ReaderContract.State;
import com.peakbw.loanapp.db.ReaderDBHelper;
import com.peakbw.loanapp.net.ManageServerConnections;

public class EnrollmentActivity2 extends Activity {

    private static final int ACTION_TAKE_PHOTO = 1;
    private static final int ACTION_SCAN_ID = 2;
    private static final int ACTION_SCAN_CONSENT = 3;

    private static final int ID_REQUEST_CODE = 404;
    private static final int PHOTO_REQUEST_CODE = 405;
    private static final int CONSENT_REQUEST_CODE = 406;

    private String employeeInfo;
    private static EnrollmentActivity2 enrollActivity;

    public static final String STATE_DOCUMENTS = "pending_documents";
    public static final String STATE_ENROLLMENT = "pending_enrollment";
    public static final String STATE_VERIFICATION = "pending_verification";
    public static final String STATE_COMPLETE = "complete";

    private static final String ID_BITMAP_STORAGE_KEY = "idbitmap";
    private static final String PHOTO_BITMAP_STORAGE_KEY = "photobitmap";
    private static final String CONSENT_BITMAP_STORAGE_KEY = "consentbitmap";

    private static final String ID_VISIBILITY_STORAGE_KEY = "idvisibility";
    private static final String PHOTO_VISIBILITY_STORAGE_KEY = "photovisibility";
    private static final String CONSENT_VISIBILITY_STORAGE_KEY = "consentvisibility";


    private static final String CONSENT_STORAGE_KEY = "consent";
    private static final String PHOTO_STORAGE_KEY = "photo";
    private static final String ID_STORAGE_KEY = "id";

    private Bitmap idBitmap,photoBitmap,consentBitmap;


    private static String filePrefix;
    //private Button idPreviewBtn;
    private ImageView imageView,idImageView,consentImageView,photoImageView;
    private String mCurrentPhotoPath;
    public static boolean idScanned,photoCaptured,consentScanned;
    private String photo="",id="",consent="",gender,dob,phone,limit,salary,employer,employeeId,employerId,hiringDate;

    private static final String DEBUG_TAG = "EnrollmentActivity2";
    public static final String activityKey = "com.peakbw.loanapp.activities.EnrollmentActivity2";

    //private static final String JPEG_FILE_PREFIX = EmployerActivity.employeeId;
    private static final String JPEG_FILE_SUFFIX = ".jpg";

    private String photoName;

    private AlbumStorageDirFactory mAlbumStorageDirFactory = null;

    public static String state;

    private  ContentValues values;
    private  SQLiteDatabase db;
    private ReaderDBHelper rDbHelper;

    private int picker_icon,imageViewId;

    private String [] picsArray;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_enrollment_activity2);
        // Show the Up button in the action bar.
        setupActionBar();

        setTitle("Subscriber Documents");

        rDbHelper = new ReaderDBHelper(getBaseContext());
        idImageView = (ImageView) findViewById(R.id.id_scan);
        consentImageView = (ImageView) findViewById(R.id.consent_scan);
        photoImageView = (ImageView) findViewById(R.id.photo_scan);


        System.out.println("onC Target Width = "+idImageView.getWidth());
        System.out.println("onC Target Height = "+idImageView.getHeight());

        Intent intent = getIntent();

        picker_icon = R.drawable.ic_input_add;

        setEmployeeFields(intent);
        setIMageViews(intent);

        values = new ContentValues();

        db = rDbHelper.getWritableDatabase();

        enrollActivity = this;

        picsArray = new String[3];

    }

    /**
     * Set up the {@link android.app.ActionBar}.
     */
    private void setupActionBar() {

        getActionBar().setDisplayHomeAsUpEnabled(true);

    }

    @Override
    public void onCreateContextMenu(ContextMenu menu, View v,ContextMenu.ContextMenuInfo menuInfo) {
        super.onCreateContextMenu(menu, v, menuInfo);
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.floating_context_menu, menu);
    }

    @Override
    public boolean onContextItemSelected(MenuItem item) {
        AdapterView.AdapterContextMenuInfo info = (AdapterView.AdapterContextMenuInfo) item.getMenuInfo();
        switch (item.getItemId()) {
            case R.id.camera:
                cameraAction(imageViewId);
                return true;
            case R.id.gallery:
                galleryAction(imageViewId);
                return true;
            case R.id.cancel:
                return true;
            default:
                return super.onContextItemSelected(item);
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        switch (item.getItemId()) {
            case android.R.id.home:
                Log.d(DEBUG_TAG, "Up button clicked!");
                finish();
                return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onStart() {
        super.onStart();
        // The activity is about to become visible.
        filePrefix = employer+"_"+employeeId+"_";

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.FROYO) {
            mAlbumStorageDirFactory = new FroyoAlbumDirFactory();
        } else {
            mAlbumStorageDirFactory = new BaseAlbumDirFactory();
        }
    }

    /*@Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu items for use in the action bar
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.enrollment_activity2, menu);
        return super.onCreateOptionsMenu(menu);
    }*/


    @Override
    protected void onResume() {
        super.onResume();
        // The activity has become visible (it is now "resumed").
    }
    @Override
    protected void onPause() {
        super.onPause();
    }
    @Override
    protected void onStop() {
        super.onStop();
        // The activity is no longer visible (it is now "stopped")
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        // The activity is about to be destroyed.
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode == RESULT_OK&&requestCode != ID_REQUEST_CODE&&requestCode !=CONSENT_REQUEST_CODE&&requestCode !=PHOTO_REQUEST_CODE) {
            handleBigCameraPhoto();
        }
        else if (resultCode == RESULT_OK && requestCode == ID_REQUEST_CODE){
            Uri uri = data.getData();
            String path = getPath(uri);
            InsertPic(path, 1);
            //picsArray[0] = path;
            id = extractName(path);
            Log.d("Image URI", "photo = "+path);

            setPic(path);

            super.onActivityResult(requestCode, resultCode, data);
        }
        else if (resultCode == RESULT_OK && requestCode == CONSENT_REQUEST_CODE){
            Uri uri = data.getData();
            String path = getPath(uri);
            InsertPic(path, 3);
            //picsArray[1] = path;
            consent = extractName(path);
            Log.d("Image URI", "photo = "+path);

            setPic(path);

            super.onActivityResult(requestCode, resultCode, data);
        }
        else if (resultCode == RESULT_OK && requestCode == PHOTO_REQUEST_CODE){
            Uri uri = data.getData();
            String path = getPath(uri);
            InsertPic(path, 2);
            //picsArray[2] = path;
            photo = extractName(path);
            Log.d("Image URI", "photo = "+path);

            setPic(path);

            super.onActivityResult(requestCode, resultCode, data);
        }
    }

    @Override
    // save activity state
    protected void onSaveInstanceState(Bundle outState) {

        outState.putParcelable(ID_BITMAP_STORAGE_KEY, idBitmap);
        outState.putBoolean(ID_VISIBILITY_STORAGE_KEY, (idBitmap != null) );

        outState.putParcelable(PHOTO_BITMAP_STORAGE_KEY, photoBitmap);
        outState.putBoolean(PHOTO_VISIBILITY_STORAGE_KEY, (photoBitmap != null) );

        outState.putParcelable(CONSENT_BITMAP_STORAGE_KEY, consentBitmap);
        outState.putBoolean(CONSENT_VISIBILITY_STORAGE_KEY, (consentBitmap != null) );

        outState.putString(ID_STORAGE_KEY,id);
        outState.putString(PHOTO_STORAGE_KEY,photo);
        outState.putString(CONSENT_STORAGE_KEY,consent);

        super.onSaveInstanceState(outState);
    }

    @Override
    //restore activity state
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);

        idBitmap = savedInstanceState.getParcelable(ID_BITMAP_STORAGE_KEY);
        photoBitmap = savedInstanceState.getParcelable(PHOTO_BITMAP_STORAGE_KEY);
        consentBitmap = savedInstanceState.getParcelable(CONSENT_BITMAP_STORAGE_KEY);


        //
        consentImageView.setImageBitmap(consentBitmap);
        consentImageView.setVisibility(
                savedInstanceState.getBoolean(CONSENT_VISIBILITY_STORAGE_KEY) ?
                        ImageView.VISIBLE : ImageView.INVISIBLE
        );
        //
        idImageView.setImageBitmap(idBitmap);
        idImageView.setVisibility(
                savedInstanceState.getBoolean(ID_VISIBILITY_STORAGE_KEY) ?
                        ImageView.VISIBLE : ImageView.INVISIBLE
        );

        //
        photoImageView.setImageBitmap(photoBitmap);
        photoImageView.setVisibility(
                savedInstanceState.getBoolean(PHOTO_VISIBILITY_STORAGE_KEY) ?
                        ImageView.VISIBLE : ImageView.INVISIBLE
        );


        id = savedInstanceState.getString(ID_STORAGE_KEY);
        consent = savedInstanceState.getString(CONSENT_STORAGE_KEY);
        photo = savedInstanceState.getString(PHOTO_STORAGE_KEY);
    }

    private void handleBigCameraPhoto() {

        if (mCurrentPhotoPath != null) {
            Log.d(DEBUG_TAG, "ImagePath = "+mCurrentPhotoPath);
            if(imageView == idImageView){
                idBitmap = setPic(mCurrentPhotoPath);
                InsertPic(mCurrentPhotoPath, 1);
            }else if(imageView == photoImageView){
                photoBitmap = setPic(mCurrentPhotoPath);
                InsertPic(mCurrentPhotoPath,2);
            }else if(imageView == consentImageView){
                consentBitmap= setPic(mCurrentPhotoPath);
                InsertPic(mCurrentPhotoPath,3);
            }
            galleryAddPic();
            mCurrentPhotoPath = null;
        }
    }

    public void enrollment2StateMachine(View view){
        if(isIntentAvailable(this, MediaStore.ACTION_IMAGE_CAPTURE)){

            imageViewId = view.getId();

            if(imageViewId ==R.id.id_scan){
                imageView = idImageView;
                registerForContextMenu(idImageView);
            }
            else if(imageViewId==R.id.consent_scan){
                imageView = consentImageView;
                registerForContextMenu(consentImageView);
            }
            else if(imageViewId==R.id.photo_scan){
                imageView = photoImageView;
                registerForContextMenu(photoImageView);
            }
            else if(imageViewId==R.id.cancel){
                EnrollmentActivity1.getInstance().finish();
                finish();
            }
            else if(imageViewId==R.id.next){
                getFormData();
            }
        }
    }

    /* Photo album for this application */
    private String getAlbumName() {
        return getString(R.string.album_name);
    }

    private String dispatchTakePictureIntent(int actionCode) {
        String fileName ="";
        switch(actionCode) {
            case ACTION_TAKE_PHOTO:
                photoName = filePrefix+"photo_";
                fileName = startCameraActivity(actionCode);
                writeToFile("fileName = "+fileName+"\n");
                Log.d("dispatchTakePictureIntent", "photo = "+fileName);
                break;

            case ACTION_SCAN_CONSENT:
                photoName = filePrefix+"consent_";
                fileName = startCameraActivity(actionCode);
                writeToFile("fileName = "+fileName+"\n");
                Log.d("dispatchTakePictureIntent", "consent = "+fileName);
                break;

            case ACTION_SCAN_ID:
                photoName = filePrefix+"id_";
                fileName = startCameraActivity(actionCode);
                writeToFile("fileName = "+fileName+"\n");
                Log.d("dispatchTakePictureIntent", "id = "+fileName);
                break;

            default:
                break;
        }
        return fileName;
    }

    private File setUpPhotoFile() throws IOException {

        File f = createImageFile();
        String file = f.getName();
        Log.d(DEBUG_TAG, "RealFileName = "+file);
        mCurrentPhotoPath = f.getAbsolutePath();

        return f;
    }

    private File createImageFile() throws IOException {
        // Create an image file name
        //String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
        String imageFileName = photoName;
        Log.d(DEBUG_TAG, "ImageName = "+imageFileName);
        File albumF = getAlbumDir();
        File imageF = File.createTempFile(imageFileName, JPEG_FILE_SUFFIX, albumF);
        return imageF;
    }

    private File getAlbumDir() {
        File storageDir = null;
        try{
            if (Environment.MEDIA_MOUNTED.equals(Environment.getExternalStorageState())) {

                storageDir = mAlbumStorageDirFactory.getAlbumStorageDir(getAlbumName());

                if (storageDir != null) {
                    if (! storageDir.mkdirs()) {
                        if (! storageDir.exists()){
                            writeToFile("Camera failed to create directory\n");
                            Log.d("CameraSample", "failed to create directory");
                            return null;
                        }
                    }
                }

            }
            else {
                writeToFile("External storage is not mounted READ/WRITE.\n");
                Log.v(getString(R.string.app_name), "External storage is not mounted READ/WRITE.");
            }
        }catch(Exception ex){
            writeToFile("Excepion = "+ex.getMessage()+"\n");
        }

        return storageDir;
    }

    public static boolean isIntentAvailable(Context context, String action) {
        final PackageManager packageManager = context.getPackageManager();
        final Intent intent = new Intent(action);

        List<ResolveInfo> list = packageManager.queryIntentActivities(intent,PackageManager.MATCH_DEFAULT_ONLY);

        return list.size() > 0;
    }

    private void galleryAddPic() {
        writeToFile("Adding pic to gallary\n");
        Intent mediaScanIntent = new Intent("android.intent.action.MEDIA_SCANNER_SCAN_FILE");
        writeToFile("Media Scanned\n");
        File f = new File(mCurrentPhotoPath);
        writeToFile("file created\n");
        Uri contentUri = Uri.fromFile(f);
        mediaScanIntent.setData(contentUri);
        this.sendBroadcast(mediaScanIntent);
        writeToFile("sent broadcast mediaScanItent\n");

        resizePicture(contentUri);
    }

    private void resizePicture(Uri picUri){

        try{
            //String newPicPath = (picUri.getPath()).replace(".jpg",".png");
            File file = new File(picUri.getPath());

            BitmapFactory.Options options=new BitmapFactory.Options();

            InputStream is = getContentResolver().openInputStream(picUri);

            Bitmap bitmap = BitmapFactory.decodeStream(is, null, options);

            is.close();

            int width = bitmap.getWidth();
            int height = bitmap.getHeight();

            int newWidth = 640;
            int newHeight = 480;

            if(height>width){
                newWidth = 480;
                newHeight =640;
            }

            float scaleWidth = ((float) newWidth) / width;
            float scaleHeight = ((float) newHeight) / height;

            Matrix matrix = new Matrix();
            matrix.postScale(scaleWidth, scaleHeight);

            Bitmap resizedBitMap = Bitmap.createBitmap( bitmap,0, 0,width, height, matrix, true);
            file.createNewFile();
            FileOutputStream ostream = new FileOutputStream(file);
            resizedBitMap.compress(Bitmap.CompressFormat.JPEG, 100, ostream);
            ostream.close();

        }catch(FileNotFoundException ex){
            ex.printStackTrace();
        }
        catch (IOException ex){
            ex.printStackTrace();
        }
    }

    private Bitmap setPic(String photoPath) {
		/* There isn't enough memory to open up more than a couple camera photos */
		/* So pre-scale the target bitmap into which the file is decoded */

		/* Get the size of the ImageView */
        int targetW = 300;
        int targetH = 300;

        //System.out.println("Width = "+targetW);
        //System.out.println("Height = "+targetH);

		/* Get the size of the image */
        BitmapFactory.Options bmOptions = new BitmapFactory.Options();
        bmOptions.inJustDecodeBounds = true;
        BitmapFactory.decodeFile(photoPath, bmOptions);

        int photoW = bmOptions.outWidth;
        int photoH = bmOptions.outHeight;
		
		/* Figure out which way needs to be reduced less */
        // Determine how much to scale down the image
        int scaleFactor = Math.min(photoW/targetW, photoH/targetH);

		/* Set bitmap options to scale the image decode target */
        bmOptions.inJustDecodeBounds = false;
        bmOptions.inSampleSize = scaleFactor;
        bmOptions.inPurgeable = true;
	    
	    /* Decode the JPEG file into a Bitmap */
        Bitmap bitmap = BitmapFactory.decodeFile(photoPath, bmOptions);
		
		/* Associate the Bitmap to the ImageView */
        imageView.setImageBitmap(bitmap);
        imageView.setVisibility(View.VISIBLE);
        writeToFile("Pic set complete\n");

        return bitmap;
    }

    private String startCameraActivity(int actionCode){
        String fileName ="";
        Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        File f = null;
        try {
            f = setUpPhotoFile();
            fileName = f.getName();
            Log.d("startCameraActivity", "takenPhoto = "+fileName);
            mCurrentPhotoPath = f.getAbsolutePath();
            writeToFile("filePath = "+mCurrentPhotoPath+"\n");
            takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, Uri.fromFile(f));
            startActivityForResult(takePictureIntent, actionCode);
        }
        catch (IOException e) {
            writeToFile("Exception = "+e.getMessage()+"\n");
            e.printStackTrace();
            f = null;
            mCurrentPhotoPath = null;
        }
        return fileName;
    }

    private void getFormData(){

        if(!consent.equals("0")){
            consentScanned = true;
        }
        else{
            consentScanned = false;
            insertIntoStateTable(STATE_DOCUMENTS);
        }

        if(!id.equals("0")){
            idScanned = true;
        }
        else{
            idScanned = false;
            insertIntoStateTable(STATE_DOCUMENTS);
        }

        if(!photo.equals("0")){
            photoCaptured = true;
            insertIntoStateTable(STATE_ENROLLMENT);
        }
        else{
            photoCaptured = false;
            insertIntoStateTable(STATE_DOCUMENTS);
        }

        startConfirmation();
    }

    private void startConfirmation(){
        Intent confirmationIntent = new Intent(this,ConfirmationActivity.class);
        confirmationIntent.putExtra(EnrollmentActivity2.activityKey,createConfirmationData());
        startActivity(confirmationIntent);
    }

    private long insertIntoStateTable(String state){
        long entryId;

        values.clear();
        Log.d(DEBUG_TAG,"My Employee ID ="+employeeId);
        values.put(State.EMPLOYEE_ID, employeeId);
        values.put(State.EMPLOYER_ID, employerId);
        values.put(State.DOB, dob);
        values.put(State.PHONE_NUMBER, phone);
        values.put(State.GENDER, gender);
        values.put(State.HIRING_DATE,hiringDate );
        Timestamp ts = new Timestamp(System.currentTimeMillis());
        values.put(State.TIME_STAMP, ts.toString());
        values.put(State.CAPTURED_ID, idScanned);
        values.put(State.CAPTURED_CONSENT, consentScanned);
        values.put(State.CAPTURED_PHOTO, photoCaptured);
        values.put(State.NET_SALARY, salary);
        values.put(State.BORROWING_LIMIT, limit);
        values.put(State.ID_FILE, id);
        values.put(State.CONSENT_FILE, consent);
        values.put(State.PHOTO_FILE, photo);
        values.put(State.STATUS, state);
        values.put(State.PUSHER, MainActivity.pin);

        // Insert the new row, returning the primary key value of the new row
        try{
            entryId = db.insertOrThrow(State.TABLE_NAME, State.COLUMN_NAME_NULLABLE, values);
        }
        catch(SQLException ex){// if exists, update row
            String selection = State.PHONE_NUMBER + " = ?";
            String[] selectionArgs = {phone};

            entryId = db.update(State.TABLE_NAME,values,selection,selectionArgs);
        }

        ///entryId = db.insertWithOnConflict(State.TABLE_NAME, State.COLUMN_NAME_NULLABLE, values, SQLiteDatabase.CONFLICT_IGNORE);

        Log.d(DEBUG_TAG, "New State Row Id:"+ entryId+" "+employeeId);
        values.clear();

        return entryId;
    }
    //insert picture absolute paths into pics table
    private void InsertPic(String path,int docType){
        values.clear();
        values.put(ReaderContract.Pics.MSISDN, phone);
        values.put(ReaderContract.Pics.NAME, path);
        values.put(ReaderContract.Pics.DOCUMENT_TYPE, docType);
        values.put(ReaderContract.Pics.SYNCED, false);
        Timestamp ts = new Timestamp(System.currentTimeMillis());
        values.put(ReaderContract.Pics.TS,ts.toString());
        db.insert(ReaderContract.Pics.TABLE_NAME,ReaderContract.Pics.COLUMN_NAME_NULLABLE,values);
        values.clear();
    }

    private void writeToFile(String content){
        OutputStreamWriter outWriter;
        try {
            if(isExternalStorageWritable()==true&&isExternalStorageReadable()==true){
                String path = getAlbumStorageDir("loanAppAlbum")+"/log.txt";
				/*outputStream = openFileOutput(path, Context.MODE_PRIVATE);
				Log.d("Hudson Bogere", outputStream.toString());
				outputStream.write(content.getBytes());
				outputStream.close();*/

                File myFile = new File(path);
                if(myFile.exists()){
                    FileOutputStream fOut = new FileOutputStream(myFile);
                    outWriter = new OutputStreamWriter(fOut);
                    outWriter.append(content);
                    outWriter.close();
                    fOut.close();
                }
                else{
                    myFile.createNewFile();
                    FileOutputStream fOut = new FileOutputStream(myFile);
                    outWriter = new OutputStreamWriter(fOut);
                    outWriter.append(content);
                    outWriter.close();
                    fOut.close();
                }
            }
            else{
                Toast.makeText(EnrollmentActivity2.this, "External Storage not writable!", Toast.LENGTH_SHORT).show();
                Log.d(DEBUG_TAG, "External Storage not readable ");
            }
        }
        catch (Exception e) {
            Log.d(DEBUG_TAG, "Error " + e.toString());
            e.printStackTrace();
        }
    }

    /* Checks if external storage is available for read and write */
    private boolean isExternalStorageWritable() {
        String state = Environment.getExternalStorageState();
        if (Environment.MEDIA_MOUNTED.equals(state)) {
            return true;
        }
        return false;
    }

    /* Checks if external storage is available to at least read */
    private boolean isExternalStorageReadable() {
        String state = Environment.getExternalStorageState();
        if (Environment.MEDIA_MOUNTED.equals(state) ||
                Environment.MEDIA_MOUNTED_READ_ONLY.equals(state)) {
            return true;
        }
        return false;
    }

    private File getAlbumStorageDir(String albumName) {
        // Get the directory for the user's public files directory.
        File file = new File(Environment.getExternalStoragePublicDirectory(
                Environment.DIRECTORY_PICTURES), albumName);
        if (!file.mkdirs()) {
            Log.e(DEBUG_TAG, "Directory not created");
        }
        return file;
    }

    private void setEmployeeFields(Intent intent){
        if(intent.hasExtra(EnrollmentActivity1.activityKey)){
            employeeInfo = intent.getStringExtra(EnrollmentActivity1.activityKey);
            Log.d(DEBUG_TAG,"Emp info = "+employeeInfo);
            ArrayList<String> emppDetails = ManageServerConnections.split(employeeInfo, "*");
            this.hiringDate = emppDetails.get(9);
            this.employer = emppDetails.get(0);
            this.employerId = emppDetails.get(7);
            this.employeeId = emppDetails.get(8);
            Log.d(DEBUG_TAG,"My Employee ID 1="+employeeId);
            this.dob = emppDetails.get(2);
            this.gender = emppDetails.get(3);
            this.phone = emppDetails.get(4);
            this.salary = emppDetails.get(6);
            this.limit = emppDetails.get(5);
        }
    }

    private void setIMageViews(Intent intent){
        if(intent.hasExtra(EmployerActivity.ID_FILE)){
            id = intent.getStringExtra(EmployerActivity.ID_FILE);
        }

        if(intent.hasExtra(EmployerActivity.CONSENT_FILE)){
            consent = intent.getStringExtra(EmployerActivity.CONSENT_FILE);
        }

        if(intent.hasExtra(EmployerActivity.PHOTO_FILE)){
            photo = intent.getStringExtra(EmployerActivity.PHOTO_FILE);
        }

        if(intent.hasExtra(EmployerActivity.ID_SCANNED)){
            String isIdScanned = intent.getStringExtra(EmployerActivity.ID_SCANNED);
            if(isIdScanned.equals("false")){
                idScanned = false;
                idBitmap = BitmapFactory.decodeResource(getBaseContext().getResources(),picker_icon);
            }
            else{
                idScanned = true;
                idBitmap = setPicToImageView(id, idImageView);
            }
        }

        if(intent.hasExtra(EmployerActivity.CONSENT_SCANNED)){
            String isConsentScanned = intent.getStringExtra(EmployerActivity.CONSENT_SCANNED);
            if(isConsentScanned.equals("false")){
                consentScanned = false;
                consentBitmap = BitmapFactory.decodeResource(this.getResources(),picker_icon);
            }
            else{
                consentScanned = true;
                consentBitmap = setPicToImageView(consent, consentImageView);
            }
        }

        if(intent.hasExtra(EmployerActivity.PHOTO_CAPTURED)){
            String isPhotoCaptured = intent.getStringExtra(EmployerActivity.PHOTO_CAPTURED);
            if(isPhotoCaptured.equals("false")){
                photoCaptured = false;
                photoBitmap = BitmapFactory.decodeResource(this.getResources(),picker_icon);
            }
            else{
                photoCaptured = true;
                photoBitmap = setPicToImageView(photo, photoImageView);
            }
        }
    }

    private String createConfirmationData(){
        String data = employeeInfo+"*"+idScanned+"*"+consentScanned+"*"+
                photoCaptured+"*"+id+"*"+consent+"*"+photo;

        return data;
    }

    public static EnrollmentActivity2 getInstance(){
        return enrollActivity;
    }

    private Bitmap setPicToImageView(String fileName, ImageView imageView) {
		/* There isn't enough memory to open up more than a couple camera photos */
		/* So pre-scale the target bitmap into which the file is decoded */

		/* Get the size of the ImageView */
        int targetW = imageView.getWidth();
        int targetH = imageView.getHeight();

        //System.out.println("Target Width = "+targetW);
        //System.out.println("Target Height = "+targetH);

        String photoPath = getAlbumStorageDir("loanAppAlbum")+"/"+fileName;
        Bitmap bitmap = null;

        System.out.println(photoPath);

		/* Get the size of the image */
        try{
            BitmapFactory.Options bmOptions = new BitmapFactory.Options();

            bmOptions.inJustDecodeBounds = true;
            BitmapFactory.decodeFile(photoPath, bmOptions);
            int photoW = bmOptions.outWidth;
            int photoH = bmOptions.outHeight;

			/* Figure out which way needs to be reduced less */
            int scaleFactor = Math.min(photoW/300, photoH/300);

			/* Set bitmap options to scale the image decode target */
            bmOptions.inJustDecodeBounds = false;
            bmOptions.inSampleSize = scaleFactor;
            bmOptions.inPurgeable = true;

			/* Decode the JPEG file into a Bitmap */
            bitmap = BitmapFactory.decodeFile(photoPath, bmOptions);

			/* Associate the Bitmap to the ImageView */
            imageView.setImageBitmap(bitmap);
            imageView.setVisibility(View.VISIBLE);
            writeToFile("Pic set complete\n");
        }
        catch(Exception ex){
            Toast.makeText(EnrollmentActivity2.this, "Images not Found!", Toast.LENGTH_SHORT).show();
            ex.printStackTrace();
        }

        return bitmap;
    }

    private String getPath(Uri uri){
        String[] filePathColumn = { MediaStore.Images.Media.DATA };

        Cursor cursor = getContentResolver().query(uri,filePathColumn, null, null, null);
        cursor.moveToFirst();

        int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
        String picturePath = cursor.getString(columnIndex);
        cursor.close();

        return picturePath;
    }

    private String extractName(String path){
        String [] files = path.split("/");
        return  files[files.length-1];
    }

    private void cameraAction(int viewId){

        if(viewId ==R.id.id_scan){
            imageView = idImageView;
            id = dispatchTakePictureIntent(ACTION_SCAN_ID);
            Log.d(DEBUG_TAG, "idScanFile = "+id);
        }
        else if(viewId==R.id.consent_scan){
            imageView = consentImageView;
            consent = dispatchTakePictureIntent(ACTION_SCAN_CONSENT);
            Log.d(DEBUG_TAG, "consentScanFile = "+consent);
        }
        else if(viewId==R.id.photo_scan){
            imageView = photoImageView;
            photo = dispatchTakePictureIntent(ACTION_TAKE_PHOTO);
            Log.d(DEBUG_TAG, "photoFile = "+photo);
        }

    }

    private void galleryAction(int viewId){

        Intent intent = new Intent();
        intent.setType("image/*");
        intent.setAction(Intent.ACTION_GET_CONTENT);
        intent.addCategory(Intent.CATEGORY_OPENABLE);

        if(viewId ==R.id.id_scan){
            //imageView = idImageView;
            startActivityForResult(intent, ID_REQUEST_CODE);
        }
        else if(viewId==R.id.consent_scan){
            //imageView = consentImageView;
            startActivityForResult(intent, CONSENT_REQUEST_CODE);
        }
        else if(viewId==R.id.photo_scan){
            //imageView = photoImageView;
            startActivityForResult(intent, PHOTO_REQUEST_CODE);
        }
    }

}
