package com.peakbw.loanapp.activities;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.DialogFragment;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.TextView;
import android.widget.Toast;
import com.peakbw.loanapp.R;
import com.peakbw.loanapp.db.ReaderContract.Employee;
import com.peakbw.loanapp.db.ReaderDBHelper;
import com.peakbw.loanapp.fragments.DatePickerFragment;

public class EnrollmentActivity1 extends Activity {

    private String gender = "MALE",dateOfBirth,netSalary,borrowingLimit;
    public static final String activityKey = "com.peakbw.loanapp.activities.EnrollmentActivity1";
    private static EnrollmentActivity1 enrollActivity1;
    private String idScanned,consentScanned,photoCaptured;
    public static EditText dateField,phoneField,limitField,salaryField,appointmentField;
    private static final String DEBURG_TAG = "Enrollment1";

    private ReaderDBHelper rDbHelper;

    public static  int appointmentView,dobView;

    private String employee,employeeId,employer,phone,employerId,idFile,consentFile,photoFile,hiringDate;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_enrollment_activity1);
        // Show the Up button in the action bar.
        setupActionBar();

        setTitle("Subscriber Details");

        dateField = (EditText) findViewById(R.id.datePicker);
        dateField.setKeyListener(null);
        dateField.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView textView, int id,KeyEvent keyEvent) {
                if (id == R.id.login || id == EditorInfo.IME_NULL) {
                    validate();
                    return true;
                }
                return false;
            }
        });

        appointmentField = (EditText) findViewById(R.id.hiringDatePicker);
        appointmentField.setKeyListener(null);
        appointmentField.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView textView, int id,KeyEvent keyEvent) {
                if (id == R.id.login || id == EditorInfo.IME_NULL) {
                    validate();
                    return true;
                }
                return false;
            }
        });

        phoneField = (EditText) findViewById(R.id.phoneNumber);
        phoneField.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView textView, int id,KeyEvent keyEvent) {
                if (id == R.id.login || id == EditorInfo.IME_NULL) {
                    validate();
                    return true;
                }
                return false;
            }
        });

        limitField = (EditText) findViewById(R.id.limit);
        limitField.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView textView, int id,KeyEvent keyEvent) {
                if (id == R.id.login || id == EditorInfo.IME_NULL) {
                    validate();
                    return true;
                }
                return false;
            }
        });

        salaryField = (EditText) findViewById(R.id.salary);
        salaryField.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView textView, int id,KeyEvent keyEvent) {
                if (id == R.id.login || id == EditorInfo.IME_NULL) {
                    validate();
                    return true;
                }
                return false;
            }
        });

        TextWatcher textWatcher = new TextWatcher() {
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                Integer inputLength = s.length();
                if(inputLength>0){
                    int limit = (int)((Integer.parseInt(s.toString()))*0.3);
                    limitField.setText(String.valueOf(limit));
                }

            }

            public void afterTextChanged(Editable s) {}
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {}
        };

        salaryField.addTextChangedListener(textWatcher);

        rDbHelper = new ReaderDBHelper(getBaseContext());

        Intent intent = getIntent();
        getDataFromPreviousActivity(intent);

        enrollActivity1 = this;

        dobView = R.id.datePicker;
        appointmentView = R.id.hiringDatePicker;
    }

    /**
     * Set up the {@link android.app.ActionBar}.
     */
    private void setupActionBar() {

        getActionBar().setDisplayHomeAsUpEnabled(true);

    }

    @Override
    protected void onStart() {
        super.onStart();
        // The activity is about to become visible.
        TextView employeeView = (TextView) findViewById(R.id.employee);

        dateField.setText(dateOfBirth);
        phoneField.setText(phone);
        limitField.setText(borrowingLimit);
        salaryField.setText(netSalary);

        employeeView.setText(employee + "-" + employeeId);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onResume() {
        super.onResume();
        // The activity has become visible (it is now "resumed").
    }
    @Override
    protected void onPause() {
        super.onPause();
        //time out the
        /*handler.postDelayed(new Runnable() {
        	@Override
        	public void run() {
        		finish();
      			}
        }, 120000 );*/
        // Another activity is taking focus (this activity is about to be "paused").
    }
    @Override
    protected void onStop() {
        super.onStop();
        // The activity is no longer visible (it is now "stopped")
    }
    @Override
    protected void onDestroy() {
        super.onDestroy();
        // The activity is about to be destroyed.
    }

    public void showDatePickerDialog(View v) {
        int id = v.getId();
        DialogFragment dateFragment = new DatePickerFragment();

        Bundle args = new Bundle();
        args.putInt("view", id);
        dateFragment.setArguments(args);

        dateFragment.show(getFragmentManager(), "Calender");;
    }

    public void next(View view){

        validate();

    }

    public void cancel(View view){
        finish();
    }

    public void onRadioButtonClicked(View view) {
        // Is the button now checked?
        boolean checked = ((RadioButton) view).isChecked();

        // Check which radio button was clicked
        switch(view.getId()) {
            case R.id.male:
                if (checked)
                    gender = "MALE";
                break;
            case R.id.female:
                if (checked)
                    gender = "FEMALE";
                break;
        }
    }

    private void startEnrollmentActivity2(){
        Intent enrollment2Intent = new Intent(this,EnrollmentActivity2.class);
        enrollment2Intent.putExtra(EnrollmentActivity1.activityKey,ceateEnrollmentData());

        enrollment2Intent.putExtra(EmployerActivity.ID_FILE,idFile);
        enrollment2Intent.putExtra(EmployerActivity.CONSENT_FILE,consentFile);
        enrollment2Intent.putExtra(EmployerActivity.PHOTO_FILE,photoFile);
        enrollment2Intent.putExtra(EmployerActivity.ID_SCANNED,idScanned);
        enrollment2Intent.putExtra(EmployerActivity.CONSENT_SCANNED,consentScanned);
        enrollment2Intent.putExtra(EmployerActivity.PHOTO_CAPTURED,photoCaptured);

        startActivity(enrollment2Intent);

        /*Intent intent = new Intent(this,CameraActivity.class);
        startActivity(intent);*/
    }

    public String ceateEnrollmentData(){
        String employeeInfo = employer+"*"+employee+"*"+dateOfBirth+"*"+gender+"*"+phone+"*"+borrowingLimit+"*"+netSalary+"*"+employerId+"*"+employeeId+"*"+hiringDate;
        return employeeInfo;
    }

    public static EnrollmentActivity1 getInstance(){
        return enrollActivity1;
    }

    private void getDataFromPreviousActivity(Intent intent){
        if(intent.hasExtra(EmployerActivity.EMPLOYEE)){
            employee = intent.getStringExtra(EmployerActivity.EMPLOYEE);
            employeeId = intent.getStringExtra(EmployerActivity.EMPLOYEE_ID);
            employer = intent.getStringExtra(EmployerActivity.EMPLOYER);
            dateOfBirth = intent.getStringExtra(EmployerActivity.DOB);
            netSalary = intent.getStringExtra(EmployerActivity.NET_SALARY);
            borrowingLimit = intent.getStringExtra(EmployerActivity.BORROWING_LIMIT);
            phone = intent.getStringExtra(EmployerActivity.PHONE);
            employerId = intent.getStringExtra(EmployerActivity.EMPLOYER_ID);

            idFile = intent.getStringExtra(EmployerActivity.ID_FILE);
            consentFile = intent.getStringExtra(EmployerActivity.CONSENT_FILE);
            photoFile = intent.getStringExtra(EmployerActivity.PHOTO_FILE);

            idScanned = intent.getStringExtra(EmployerActivity.ID_SCANNED);
            consentScanned = intent.getStringExtra(EmployerActivity.CONSENT_SCANNED);
            photoCaptured = intent.getStringExtra(EmployerActivity.PHOTO_CAPTURED);
        }
    }

    private void createAlertDialog(String msg,int layout,int textView){
        LayoutInflater li = LayoutInflater.from(this);
        View promptsView = li.inflate(layout, null);
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);
        alertDialogBuilder.setView(promptsView);

        final TextView alertTextView = (TextView) promptsView.findViewById(textView);
        alertTextView.setText(msg);

        // set dialog message
        alertDialogBuilder.setCancelable(false).setPositiveButton("OK",new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog,int id) {
                startEnrollmentActivity2();
            }
        }).setNegativeButton("Cancel",new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog,int id) {
                dialog.cancel();
            }
        });

        AlertDialog alertDialog = alertDialogBuilder.create();
        alertDialog.show();
    }

    private void validate(){
        borrowingLimit = limitField.getText().toString();
        netSalary = salaryField.getText().toString();
        phone = phoneField.getText().toString();
        dateOfBirth = dateField.getText().toString();
        hiringDate = appointmentField.getText().toString();

        int salary = 0;

        boolean cancel = false;
        View focusView = null;

        if (TextUtils.isEmpty(dateOfBirth)) {
            dateField.setError(getString(R.string.error_dod));
            focusView = dateField;
            cancel = true;
        }

        if (TextUtils.isEmpty(hiringDate)) {
            appointmentField.setError(getString(R.string.error_app_dod));
            focusView = appointmentField;
            cancel = true;
        }

        if (TextUtils.isEmpty(phone)) {
            phoneField.setError(getString(R.string.error_phone));
            focusView = phoneField;
            cancel = true;
        }else{
            if(!(phone.length()==10&&(phone.substring(0, 3).equals("078")||phone.substring(0, 3).equals("077")||phone.substring(0, 3).equals("075")||phone.substring(0, 3).equals("070")))){
                phoneField.setError(getString(R.string.error_phone1));
                focusView = phoneField;
                cancel = true;
            }
        }

        if (TextUtils.isEmpty(netSalary)) {
            salaryField.setError(getString(R.string.error_salary));
            focusView = salaryField;
            cancel = true;
        }else{//check for minimum salary
            salary = Integer.parseInt(netSalary);
            if(salary<100000){
                salaryField.setError(getString(R.string.error_salary1));
                focusView = salaryField;
                cancel = true;
            }else{// compare specified Net Salary with HR's Net Salary
                Log.d(DEBURG_TAG,"EmployerID = "+employerId);
                Log.d(DEBURG_TAG,"EmployeeID = "+employeeId);
                //Cursor cursor = rDbHelper.getNetSalary(employeeId,employerId);

                //int rowCount = cursor.getCount();
                //int colIndex = cursor.getColumnIndexOrThrow(Employee.NET_SALARY);

                //Log.d(DEBURG_TAG,"Number of Rows = "+rowCount);
                //Log.d(DEBURG_TAG,"Column Index = "+colIndex);

                //cursor.moveToFirst();
                //String HR_NetSalary = cursor.getString(colIndex);

                //int HR_Salary = 0;

                //Log.d(DEBURG_TAG,"HR NetSalary = "+HR_NetSalary);

                //if(HR_NetSalary.length()>0){
                //    HR_Salary = Integer.parseInt(HR_NetSalary);
                //}

                //if(salary>HR_Salary){
                //    salaryField.setError(getString(R.string.error_salary2));
                //    focusView = salaryField;
                //    cancel = true;
                //}
            }
        }

        if (TextUtils.isEmpty(borrowingLimit)) {
            limitField.setError(getString(R.string.error_limit));
            focusView = limitField;
            cancel = true;
        }else{
            if(Integer.parseInt(borrowingLimit)>(0.3*salary)){
                limitField.setError(getString(R.string.error_limit1));
                focusView = limitField;
                cancel = true;
            }
        }

        if (cancel) {
            // There was an error; don't attempt continue and focus the first
            // form field with an error.
            focusView.requestFocus();
        } else {
            Cursor cursor = rDbHelper.checkPhoneNumber(phone);
            //check if phone number was not used by another person
            if(cursor != null&&cursor.moveToFirst()==true){
                String name = cursor.getString(cursor.getColumnIndexOrThrow(Employee.EMPLOYEE_NAME));
                String id = cursor.getString(cursor.getColumnIndexOrThrow(Employee.EMPLOYEE_ID));

                //Toast.makeText(EnrollmentActivity1.this,phone+" was used by "+name+"-"+id, Toast.LENGTH_SHORT).show();

                createAlertDialog(phone+" was used by "+name+"-"+id+". \nDo you want to do an update?",R.layout.confirmation_prompt,R.id.message);
            }
            else{
                startEnrollmentActivity2();
            }
        }

    }
}
