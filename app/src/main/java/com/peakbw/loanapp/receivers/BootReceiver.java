package com.peakbw.loanapp.receivers;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

import com.peakbw.loanapp.services.FTPService;

public class BootReceiver extends BroadcastReceiver {
    public BootReceiver() {
    }

    @Override
    public void onReceive(Context context, Intent intent) {
        // TODO: This method is called when the BroadcastReceiver is receiving
        // an Intent broadcast.
        System.out.println("intent Recieved");
        Intent serviceIntent = new Intent(context, FTPService.class);
        System.out.println("Service Created");
        context.startService(serviceIntent);
        System.out.println("Service Started");
    }
}
