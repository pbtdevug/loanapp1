package com.peakbw.loanapp.db;

import java.util.Date;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;
import com.peakbw.loanapp.db.ReaderContract.Auth;
import com.peakbw.loanapp.db.ReaderContract.Employee;
import com.peakbw.loanapp.db.ReaderContract.Employer;
import com.peakbw.loanapp.db.ReaderContract.Signature;
import com.peakbw.loanapp.db.ReaderContract.State;

public class ReaderDBHelper extends SQLiteOpenHelper {
	
	// If you change the database schema, you must increment the database version.
    public static final int DATABASE_VERSION = 1;
    public static final String DATABASE_NAME = "loanDatabase.db";
    public static SQLiteDatabase wDB,rDB;
    public static final String DEBUG_TAG = "ReaderDBHelper";
	private static final String TEXT_TYPE = " TEXT";
	private static final String DATE = " DATE";
	private static final String TIMESTAMP = " TIMESTAMP";
	private static final String COMMA_SEP = ",";
	private static ContentValues values = new ContentValues();
    
    private static final String SQL_CREATE_EMPLOYEE =
    	    "CREATE TABLE " + Employee.TABLE_NAME + " (" +
    	    		Employee._ID + " INTEGER PRIMARY KEY NOT NULL," +
    	    		Employee.EMPLOYEE_ID +TEXT_TYPE + COMMA_SEP+
                    Employee.EMPLOYER_ID + TEXT_TYPE + COMMA_SEP+
    	    		Employee.EMPLOYEE_NAME + TEXT_TYPE + COMMA_SEP+
                    Employee.NET_SALARY +TEXT_TYPE + COMMA_SEP+
                    " CONSTRAINT uc_employeeID UNIQUE ("+Employee.EMPLOYEE_ID+","+Employee.EMPLOYER_ID+")" +
            ")";
    
    private static final String SQL_DELETE_EMPLOYEE ="DROP TABLE IF EXISTS " + Employee.TABLE_NAME;
    
    private static final String SQL_CREATE_EMPLOYER =
    	    "CREATE TABLE " + Employer.TABLE_NAME + " (" +
    	    		Employer._ID + " INTEGER PRIMARY KEY," + 
    	    		Employer.EMPLOYER_ID +TEXT_TYPE+ " UNIQUE NOT NULL," +
    	    		Employer.EMPLOYER_NAME + TEXT_TYPE + ")";
    
    private static final String SQL_DELETE_EMPLOYER ="DROP TABLE IF EXISTS " + Employer.TABLE_NAME;
    
    private static final String SQL_CREATE_AUTH =
    	    "CREATE TABLE " + Auth.TABLE_NAME + " (" +
    	    		Auth._ID + " INTEGER PRIMARY KEY," + 
    	    		Auth.USER_PIN + " INTEGER UNIQUE NOT NULL," +
    	    		Auth.USER_NAME + TEXT_TYPE + ")";
    
    private static final String SQL_DELETE_AUTH ="DROP TABLE IF EXISTS " + Auth.TABLE_NAME;
    
    private static final String SQL_CREATE_STATE =
    	    "CREATE TABLE " + State.TABLE_NAME + " (" +
    	    		State._ID + " INTEGER PRIMARY KEY," + 
    	    		State.EMPLOYEE_ID + " INTEGER," +
    	    		State.EMPLOYER_ID + " INTEGER" + COMMA_SEP+
    	    		State.PHONE_NUMBER + TEXT_TYPE + " UNIQUE NOT NULL,"+
    	    		State.DOB + DATE + COMMA_SEP +
                    State.HIRING_DATE + DATE + COMMA_SEP +
    	    		State.GENDER + TEXT_TYPE + COMMA_SEP +
    	    		State.CAPTURED_ID + " BOOLEAN,"+
    	    		State.CAPTURED_CONSENT +" BOOLEAN,"+
    	    		State.CAPTURED_PHOTO + " BOOLEAN," +
    	    		State.NET_SALARY + " INTEGER," +
    	    		State.BORROWING_LIMIT + " INTEGER," +
    	    		State.ID_FILE + TEXT_TYPE + COMMA_SEP +
    	    		State.CONSENT_FILE + TEXT_TYPE + COMMA_SEP +
    	    		State.PHOTO_FILE + TEXT_TYPE + COMMA_SEP +
    	    		State.TIME_STAMP + " DATETIME" + COMMA_SEP +
    	    		State.STATUS + TEXT_TYPE + COMMA_SEP +
    	    		State.PUSHER + TEXT_TYPE +")";
    
    private static final String SQL_DELETE_STATE ="DROP TABLE IF EXISTS " + State.TABLE_NAME;

    private static final String SQL_CREATE_SIGNATURE =
    	    "CREATE TABLE " + Signature.TABLE_NAME + " (" +
    	    		Signature._ID + " INTEGER PRIMARY KEY," + 
    	    		Signature.SIGNATURE + TEXT_TYPE + COMMA_SEP +
                    Signature.UPDATE_URL+ TEXT_TYPE + COMMA_SEP +
    	    		Signature.DATE + TEXT_TYPE + ")";
    
    private static final String SQL_DELETE_SIGNATURE ="DROP TABLE IF EXISTS " + Signature.TABLE_NAME;

    private static final String SQL_CREATE_PICS =
            "CREATE TABLE " + ReaderContract.Pics.TABLE_NAME + " (" +
                    ReaderContract.Pics._ID + " INTEGER PRIMARY KEY," +
                    ReaderContract.Pics.NAME + TEXT_TYPE + ", "+
                    ReaderContract.Pics.DOCUMENT_TYPE + " INTEGER" + ", "+
                    ReaderContract.Pics.MSISDN + " INTEGER," +
                    ReaderContract.Pics.TS + " DATETIME," +
                    ReaderContract.Pics.SYNCED + " BOOLEAN "+")";

    private static final String SQL_DELETE_PICS ="DROP TABLE IF EXISTS " + ReaderContract.Pics.TABLE_NAME;
    
    public ReaderDBHelper(Context context) {
		super(context, DATABASE_NAME, null, DATABASE_VERSION);
		ReaderDBHelper.wDB = getWritableDatabase();
        ReaderDBHelper.rDB = getReadableDatabase();
	}

	@Override
	public void onCreate(SQLiteDatabase db) {
		//String pin = MainActivity.pin;
		db.execSQL(SQL_CREATE_EMPLOYEE);
		Log.d(DEBUG_TAG, "SQL for Employee:"+ SQL_CREATE_EMPLOYEE);
		db.execSQL(SQL_CREATE_EMPLOYER);
		Log.d(DEBUG_TAG, "SQL for Employer:"+ SQL_CREATE_EMPLOYER);
		db.execSQL(SQL_CREATE_AUTH);
		Log.d(DEBUG_TAG, "SQL for Auth:"+ SQL_CREATE_AUTH);
        db.execSQL(SQL_CREATE_PICS);
        Log.d(DEBUG_TAG, "SQL for PICS:" + SQL_CREATE_PICS);
		//insertFirstUser(pin);
		db.execSQL(SQL_CREATE_STATE);
		Log.d(DEBUG_TAG, "SQL for State:"+ SQL_CREATE_STATE);
		db.execSQL(SQL_CREATE_SIGNATURE);
		Log.d(DEBUG_TAG, "SQL for Signature:"+ SQL_CREATE_SIGNATURE);
		insertFirstSignature(db);

	}

	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
		db.execSQL(SQL_DELETE_EMPLOYEE);
		db.execSQL(SQL_DELETE_EMPLOYER);
		db.execSQL(SQL_DELETE_AUTH);
		db.execSQL(SQL_DELETE_STATE);
		db.execSQL(SQL_DELETE_SIGNATURE);
        db.execSQL(SQL_DELETE_PICS);
		onCreate(db);
	}
	
	/** Returns all the employers in the table */
    public Cursor getAllEmpoyers(){
    	Cursor c = rDB.query(Employer.TABLE_NAME, new String[] {Employer._ID, Employer.EMPLOYER_ID,Employer.EMPLOYER_NAME} ,
                            null, null, null, null,
                            Employer.EMPLOYER_NAME + " asc ");
		
    	
    	return c;
    }
    
    /** Returns all the employers in the table */
    public Cursor getAllEmpoyees(){
    	Cursor c = rDB.query(Employee.TABLE_NAME, new String[] {Employee._ID, Employee.EMPLOYEE_ID,Employee.EMPLOYEE_NAME,Employee.EMPLOYER_ID} ,
                            null, null, null, null,
                            Employee.EMPLOYEE_NAME + " asc ");
		
    	
    	return c;
    }
    
    public Cursor getSignature(){
    	Cursor c = rDB.query(Signature.TABLE_NAME, new String[] {Signature._ID, Signature.SIGNATURE,Signature.UPDATE_URL} ,
    			null, null, null, null,null);
    	return c;
    }
    
    public Cursor getEmployerID(String employeeID,String employeeName){
    	    	
    	Cursor c = rDB.rawQuery("SELECT ee.employerId FROM employee ee WHERE ee.idi = ? AND ee.employeeName = ?;", new String[]{employeeID,employeeName});
    	return c;
    }
    
    public Cursor getEmployeeName(String id,String employer){
    	//Cursor c = mDB.query(Employee.TABLE_NAME, new String[] {Employee._ID, Employee.EMPLOYEE_NAME} ,
    			//Employee.EMPLOYEE_ID+" = ?", new String[]{String.valueOf(id)} , null,null,null);
        Log.d(DEBUG_TAG, "Employee"+id+"-"+employer);
    	
    	Cursor c = rDB.rawQuery("SELECT employeeName FROM employee  WHERE employerId = ? AND idi = ?;", new String[]{employer,id});
    	return c;
    }
    
    public Cursor getHistory(){
    	Cursor c = rDB.rawQuery("SELECT ee._ID, ee.employeeName,ee.idi,s.state FROM employee ee JOIN state s ON ee.id = s.employeeId;", null);
    	return c;
    }

    public Cursor getHistoryTrxns(String state){
        String query = "SELECT * FROM employee ee ";
        query += " JOIN state s ON ee.idi = s.employeeId JOIN employer p ON ee.employerId = p.id JOIN pics i on s.phoneNumber = i.msisdn ";
        query += " WHERE s.state = ? AND i.doc_type = ? ORDER BY s.timeStamp DESC;";
        Cursor c = rDB.rawQuery(query, new String[]{state,"2"});
        return c;
    }

    public Cursor getDocuments(){
        String query = "SELECT e.employeeName,p.name,p.synced,p.ts FROM pics p JOIN state s ON p.msisdn = s.phoneNumber JOIN employee e ON e.idi = s.employeeId ORDER BY p.ts DESC;";
        Cursor c = rDB.rawQuery(query, null);
        return c;
    }
    
    public Cursor getEmployeeHistory(String employeeID,String employerID){
    	Cursor c = rDB.rawQuery("SELECT e.employerName, ee.employeeName,ee.id,e.id,s.employerId,s.employeeId,s.phoneNumber,s.dob,s.gender,s.netSalary,s.borrowingLimit FROM employee ee JOIN state s ON ee.idi = s.employeeId JOIN employer e ON e.id = ee.employerId WHERE ee.idi = ? AND ee.employerId = ?;", new String[]{employeeID,employerID});
    	return c;
    }
    
    public Cursor getEnrollmentInfo(String employeeID,String employerID){
    	Cursor c = rDB.rawQuery("SELECT e.employerName, ee.employeeName,ee.id,e.id,s.phoneNumber,s.dob,s.gender,s.netSalary,s.borrowingLimit,s.capturedConsent,s.capturedId,s.capturedPhoto,s.idFile,s.consentFile,s.photoFile,s.state FROM employee ee JOIN state s ON ee.idi = s.employeeId JOIN employer e ON e.id = ee.employerId WHERE ee.idi= ? AND ee.employerId = ?;", new String[]{employeeID,employerID});
    	return c;
    }
    
    private void insertFirstSignature(SQLiteDatabase db){
    	values.put(Signature.SIGNATURE, "0");
    	values.put(Signature.DATE, (new Date()).toString());
        values.put(Signature.UPDATE_URL, "url");
    	
    	// Insert the new row, returning the primary key value of the new row
    	long newRowId = db.insertWithOnConflict(
    			Signature.TABLE_NAME,
    			Signature.COLUMN_NAME_NULLABLE,
    			values,
    			SQLiteDatabase.CONFLICT_REPLACE);
    	Log.d(DEBUG_TAG, "New Signature Row Id:"+ newRowId+" "+"0");
    	Log.d(DEBUG_TAG, "New Signature: "+ "Signature"+"0");
    	values.clear();
    }

    public Cursor checkPhoneNumber(String phoneNumber){
    	Cursor c = rDB.rawQuery("SELECT ee.employeeName,ee.idi FROM employee ee JOIN state s ON ee.idi = s.employeeId WHERE s.phoneNumber = ?;", new String[]{phoneNumber});
    	return c;
    }

    public  Cursor getPics(){
        Cursor cursor = rDB.rawQuery("SELECT name FROM pics WHERE synced = 0;", null);
        return  cursor;
    }

    public Cursor getNetSalary(String employeeId,String employerId){
        Cursor cursor = rDB.rawQuery("SELECT netSalary FROM employee WHERE idi = ? AND employerId = ?;",new String[]{employeeId,employerId});
        return  cursor;
    }
}
