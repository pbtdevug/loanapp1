package com.peakbw.loanapp.net;

import java.io.IOException;
import java.io.InputStream;
import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;
import com.peakbw.loanapp.activities.MainActivity;
import com.peakbw.loanapp.db.ReaderContract.Auth;
import com.peakbw.loanapp.db.ReaderContract.Employee;
import com.peakbw.loanapp.db.ReaderContract.Employer;
import com.peakbw.loanapp.db.ReaderContract.Signature;
import com.peakbw.loanapp.db.ReaderDBHelper;
import android.content.ContentValues;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;
import android.util.Xml;

public class XMLParser {
	private static final String DEBUG_TAG = "XMLParser";
    private static final String ns = null;
    private static String date;
    private static ReaderDBHelper rDbHelper = MainActivity.rDbHelper;
    public static String signature = new String(),status,confirmationNum,trxnNum,guardName = "",method,upgradeURL,version;
	private static ContentValues values = new ContentValues();
	private static SQLiteDatabase db;


	
	public static String parseXML(InputStream is){//throws XmlPullParserException, IOException
		

        
        db = rDbHelper.getWritableDatabase();
        
        status = null;confirmationNum=null;trxnNum=null;method=null;
        
		try{
			

			/*int ch ;
	        StringBuffer sb = new StringBuffer();
	        while((ch = is.read())!=-1){
	            sb.append((char)ch);
	        }
	        Log.d(DEBUG_TAG, "Reply XML= "+sb.toString());*/


        	XmlPullParser parser = Xml.newPullParser();
        	parser.setFeature(XmlPullParser.FEATURE_PROCESS_NAMESPACES, false);
        	parser.setInput(is, null);
        	parser.nextTag();
        	parser.require(XmlPullParser.START_TAG, ns, "pbtReply");
        	
        	String employeeId = new String(),action = new String(), 
        			employerName = new String(),employerId = new String(),
        			employeeName = new String(), netSalary = new String();
        	
        	while (parser.next() != XmlPullParser.END_TAG) {
        		if (parser.getEventType() != XmlPullParser.START_TAG) {
        			continue;
        		}
        		String name = parser.getName();
        		Log.d(DEBUG_TAG, "TagName: "+name);
        		
        		if(name.equals("date")){
        			parser.require(XmlPullParser.START_TAG, ns, "date");
        			if (parser.next() == XmlPullParser.TEXT) {
        				date = (parser.getText()).substring(0,10);
        		        parser.nextTag();
        		    }
        			parser.require(XmlPullParser.END_TAG, ns, "date");
        			Log.d(DEBUG_TAG, "Date: "+date);
        		}
        		else if(name.equals("signature")){
        			parser.require(XmlPullParser.START_TAG, ns, "signature");
        			if (parser.next() == XmlPullParser.TEXT) {
        				signature = parser.getText();
        				//update signature
        		        parser.nextTag();
        		    }
        			parser.require(XmlPullParser.END_TAG, ns, "signature");
        			Log.d(DEBUG_TAG, "Signature: "+signature);
        		}
        		else if(name.equalsIgnoreCase("status")){
        			parser.require(XmlPullParser.START_TAG, ns, name);
        			if (parser.next() == XmlPullParser.TEXT) {
        				status = parser.getText();
        		        parser.nextTag();
        		    }
        			parser.require(XmlPullParser.END_TAG, ns, name);
        			Log.d(DEBUG_TAG, "Status: "+status);
        		}
        		else if(name.equals("confNumber")){
        			parser.require(XmlPullParser.START_TAG, ns, "confNumber");
        			if (parser.next() == XmlPullParser.TEXT) {
        				confirmationNum = parser.getText();
        		        parser.nextTag();
        		    }
        			parser.require(XmlPullParser.END_TAG, ns, "confNumber");
        			Log.d(DEBUG_TAG, "ConfNum: "+confirmationNum);
        		}
        		else if(name.equals("refNumber")){
        			parser.require(XmlPullParser.START_TAG, ns, "refNumber");
        			if (parser.next() == XmlPullParser.TEXT) {
        				trxnNum = parser.getText();
        		        parser.nextTag();
        		    }
        			parser.require(XmlPullParser.END_TAG, ns, "refNumber");
        			Log.d(DEBUG_TAG, "refNumber: "+trxnNum);
        		}
        		else if(name.equals("method")){
        			parser.require(XmlPullParser.START_TAG, ns, "method");
        			if (parser.next() == XmlPullParser.TEXT) {
        				method = parser.getText();
        		        parser.nextTag();
        		    }
        			parser.require(XmlPullParser.END_TAG, ns, "method");
        			Log.d(DEBUG_TAG, "Method: "+method);
        		}
        		else if(name.equals("upgradeURL")){
        			parser.require(XmlPullParser.START_TAG, ns, "upgradeURL");
        			if (parser.next() == XmlPullParser.TEXT) {
        				upgradeURL = parser.getText();
        		        parser.nextTag();
        		    }
        			parser.require(XmlPullParser.END_TAG, ns, "upgradeURL");
        			Log.d(DEBUG_TAG, "upgradeURL: "+upgradeURL);
        		}
        		else if(name.equals("version")){
        			parser.require(XmlPullParser.START_TAG, ns, "version");
        			if (parser.next() == XmlPullParser.TEXT) {
        				version = parser.getText();
        		        parser.nextTag();
        		    }
        			parser.require(XmlPullParser.END_TAG, ns, "version");
        			Log.d(DEBUG_TAG, "version: "+version);
        		}
        		else if (name.equals("update")) {
        			parser.require(XmlPullParser.START_TAG, ns, "update");
        			while (parser.next() != XmlPullParser.END_TAG) {
        				if (parser.getEventType() != XmlPullParser.START_TAG) {
        	                continue;
        	            }
        				String name1 = parser.getName();
        				if (name1.equals("employers")) {
        					parser.require(XmlPullParser.START_TAG, ns, "employers");
        					while(parser.next()!= XmlPullParser.END_TAG){
        						if (parser.getEventType() != XmlPullParser.START_TAG) {
                	                continue;
                	            }
        						String name2 = parser.getName();
        						if (name2.equals("employer")) {
        							parser.require(XmlPullParser.START_TAG, ns, "employer");
        		        			while(parser.next()!= XmlPullParser.END_TAG){
        		        				if (parser.getEventType() != XmlPullParser.START_TAG) {
                        	                continue;
                        	            }
                						String name3 = parser.getName();
                						if(name3.equals("name")) {
                							parser.require(XmlPullParser.START_TAG, ns, "name");
                		        			if (parser.next() == XmlPullParser.TEXT) {
                		        				employerName = parser.getText();
                		        		        parser.nextTag();
                		        		    }
                		        			parser.require(XmlPullParser.END_TAG, ns, "name");
                		        			Log.d(DEBUG_TAG, "Employer: "+employerName);
                						}
                						else if(name3.equals("id")){
                							parser.require(XmlPullParser.START_TAG, ns, "id");
                		        			if (parser.next() == XmlPullParser.TEXT) {
                		        				employerId = parser.getText();
                		        		        parser.nextTag();
                		        		    }
                		        			parser.require(XmlPullParser.END_TAG, ns, "id");
                		        			Log.d(DEBUG_TAG, "EmployerId: "+employerId);
                						}
                						else if(name3.equals("action")){
                							parser.require(XmlPullParser.START_TAG, ns, "action");
                		        			if (parser.next() == XmlPullParser.TEXT) {
                		        				action = parser.getText();
                		        		        parser.nextTag();
                		        		    }
                		        			parser.require(XmlPullParser.END_TAG, ns, "action");
                		        			Log.d(DEBUG_TAG, "Action: "+action);
                						}
                						else{
                							skip(parser);
                						}
        		        			}
        		        			//if("INSERT".equalsIgnoreCase(action)){
                                        
        		        				// Create a new map of values, where column names are the keys
        		        		    	values.put(Employer.EMPLOYER_ID, employerId.trim());
        		        		    	values.put(Employer.EMPLOYER_NAME, employerName.trim());
        		        		    	
        		        		    	// Insert the new row, returning the primary key value of the new row
        		        		    	long newRowId = db.insertWithOnConflict(Employer.TABLE_NAME,Employer.COLUMN_NAME_NULLABLE,values,SQLiteDatabase.CONFLICT_IGNORE);

                                        Log.d(DEBUG_TAG, "New Employer Row Id:"+ newRowId+" "+employerId);
                                        Log.d(DEBUG_TAG, "New Employer: "+ employerName);

        		        		    	values.clear();
                                    /*}
                                    else if("DELETE".equalsIgnoreCase(action)){
                                    	
                                    }*/
        						}
        						else{
        							skip(parser);
        						}
        					}
        				}
        				else if (name1.equals("employees")) {
        					parser.require(XmlPullParser.START_TAG, ns, "employees");
        					while(parser.next()!= XmlPullParser.END_TAG){
        						if (parser.getEventType() != XmlPullParser.START_TAG) {
                	                continue;
                	            }
        						String name4 = parser.getName();
        						if (name4.equals("employee")) {
        							parser.require(XmlPullParser.START_TAG, ns, "employee");
        		        			while(parser.next()!= XmlPullParser.END_TAG){
        		        				if (parser.getEventType() != XmlPullParser.START_TAG) {
                        	                continue;
                        	            }
                						String name5 = parser.getName();
                						if(name5.equals("name")) {
                							parser.require(XmlPullParser.START_TAG, ns, "name");
                		        			if (parser.next() == XmlPullParser.TEXT) {
                		        				employeeName = parser.getText();
                		        		        parser.nextTag();
                		        		    }
                		        			parser.require(XmlPullParser.END_TAG, ns, "name");
                		        			//Log.d(DEBUG_TAG, "EmployeeName: "+employeeName);
                						}
                						else if(name5.equals("id")){
                							parser.require(XmlPullParser.START_TAG, ns, "id");
                		        			if (parser.next() == XmlPullParser.TEXT) {
                		        				employeeId = parser.getText();
                		        		        parser.nextTag();
                		        		    }
                		        			parser.require(XmlPullParser.END_TAG, ns, "id");
                		        			//Log.d(DEBUG_TAG, "EmployeeID: "+employeeId);
                						}
                						else if(name5.equals("employerID")){
                							parser.require(XmlPullParser.START_TAG, ns, "employerID");
                		        			if (parser.next() == XmlPullParser.TEXT) {
                		        				employerId = parser.getText();
                		        		        parser.nextTag();
                		        		    }
                		        			parser.require(XmlPullParser.END_TAG, ns, "employerID");
                		        			//Log.d(DEBUG_TAG, "employerID: "+employerId);
                						}
                                        else if(name5.equals("netSalary")){
                                            parser.require(XmlPullParser.START_TAG, ns, "netSalary");
                                            if (parser.next() == XmlPullParser.TEXT) {
                                                netSalary = parser.getText();
                                                parser.nextTag();
                                            }
                                            parser.require(XmlPullParser.END_TAG, ns, "netSalary");
                                            Log.d(DEBUG_TAG, "netSalary: "+netSalary);
                                        }
                						else if(name5.equals("action")){
                							parser.require(XmlPullParser.START_TAG, ns, "action");
                		        			if (parser.next() == XmlPullParser.TEXT) {
                		        				action = parser.getText();
                		        		        parser.nextTag();
                		        		    }
                		        			parser.require(XmlPullParser.END_TAG, ns, "action");
                		        			//Log.d(DEBUG_TAG, action);
                						}
                						else{
                							skip(parser);
                						}
        		        			}
        		        			//if("INSERT".equalsIgnoreCase(action)){
        		        				
        		        				// Create a new map of values, where column names are the keys
        		        		    	values.put(Employee.EMPLOYEE_ID, employeeId.trim());
        		        		    	values.put(Employee.EMPLOYER_ID, employerId.trim());
                                        values.put(Employee.EMPLOYEE_NAME, employeeName.trim());
                                        values.put(Employee.NET_SALARY,netSalary.trim());
        		        		    	
        		        		    	// Insert the new row, returning the primary key value of the new row
        		        		    	long newRowId = db.insertWithOnConflict(Employee.TABLE_NAME,Employee.COLUMN_NAME_NULLABLE,values,SQLiteDatabase.CONFLICT_REPLACE);
                                        if(employerId.trim().equals("9902")){
                                            Log.d(DEBUG_TAG, "New Employee Row Id:"+ newRowId+" "+employeeId);
                                            Log.d(DEBUG_TAG, "New Employee: "+ employeeName+" "+employerId);

        		        		    	}
        		        		    	values.clear();
                                    /*}
                                    else if("DELETE".equalsIgnoreCase(action)){
                                    	
                                    }*/
        						}
        						else{
        							skip(parser);
        						}
        					}
        				}
        				else {
                			skip(parser);
                		}
        			}
        		}
        		else {
        			skip(parser);
        		}
        	}
        	
        	if(status.trim().equalsIgnoreCase("SUCCESSFUL")){
				addUser();
				updateSignature();
			}
        }
        catch(IOException ex){
        	ex.printStackTrace();
        }
        catch(XmlPullParserException ex){
        	ex.printStackTrace();
        }
        finally {
        	try{
            is.close();
            }
        	catch(IOException ex){
            	ex.printStackTrace();
            }
        }
        return status.trim();
	}
	
	private static void skip(XmlPullParser parser) throws XmlPullParserException, IOException {
        if (parser.getEventType() != XmlPullParser.START_TAG) {
            throw new IllegalStateException();
        }
        int depth = 1;
        while (depth != 0) {
            switch (parser.next()) {
            case XmlPullParser.END_TAG:
                    depth--;
                    break;
            case XmlPullParser.START_TAG:
                    depth++;
                    break;
            }
        }
    }
	
	private static void addUser(){
		String pin = MainActivity.pin;
		values.clear();
		values.put(Auth.USER_PIN, Integer.parseInt(pin));
    	values.put(Auth.USER_NAME, "user"+pin);
    	
    	// Insert the new row, returning the primary key value of the new row
    	long newRowId = db.insertWithOnConflict(Auth.TABLE_NAME, Auth.COLUMN_NAME_NULLABLE, values, SQLiteDatabase.CONFLICT_IGNORE);
    	Log.d(DEBUG_TAG, "New Auth Row Id:"+ newRowId+" "+pin);
    	Log.d(DEBUG_TAG, "New User: "+ "user"+pin);
    	values.clear();
		
	}
	
	private static void updateSignature(){
		values.put(Signature.SIGNATURE, signature);
		values.put(Signature.DATE, date);
		
		int count = db.update(
		    Signature.TABLE_NAME,
		    values,
		    null,
		    null);
		
		Log.d(DEBUG_TAG, "Update= : "+count);
		values.clear();
	}
}
