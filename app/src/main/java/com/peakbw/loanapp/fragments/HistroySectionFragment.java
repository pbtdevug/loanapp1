package com.peakbw.loanapp.fragments;

import android.app.Activity;
import android.content.Context;
import android.database.Cursor;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.ListAdapter;
import android.widget.TextView;
import com.peakbw.loanapp.R;
import com.peakbw.loanapp.activities.EnrollmentActivity2;
import com.peakbw.loanapp.db.ReaderContract;
import com.peakbw.loanapp.db.ReaderDBHelper;
import com.peakbw.loanapp.util.CustomListAdapter;
import com.peakbw.loanapp.util.ListItem;
import java.util.ArrayList;
import java.util.List;

/**
 * A fragment representing a list of Items.
 * <p />
 * Large screen devices (such as tablets) are supported by replacing the ListView
 * with a GridView.
 * <p />
 * Activities containing this fragment MUST implement the OnFragmentInteractionListener
 * interface.
 */

public class HistroySectionFragment extends Fragment implements AbsListView.OnItemClickListener {

    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    public static final String ARG_PARAM1 = "param1";

    private static final String DEBURG_TAG = "HistroySectionFragment";

    // TODO: Rename and change types of parameters
    private String mParam1;

    private OnFragmentInteractionListener mListener;

    /**
     * The fragment's ListView/GridView.
     */
    private AbsListView mListView;

    /**
     * The Adapter which will be used to populate the ListView/GridView with
     * Views.
     */
    private CustomListAdapter adapter;

    private static ReaderDBHelper rDbHelper;

    List<ListItem> historyList;

    private String recordState = "";

    // TODO: Rename and change types of parameters
    public static HistroySectionFragment newInstance(Context context) {

        HistroySectionFragment fragment = new HistroySectionFragment();

        rDbHelper = new ReaderDBHelper(context);

        return fragment;
    }

    /**
     * Mandatory empty constructor for the fragment manager to instantiate the
     * fragment (e.g. upon screen orientation changes).
     */
    public HistroySectionFragment() {

    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        historyList = new ArrayList<ListItem>();

        Cursor cursor;

        if (getArguments() != null) {
            int tabIndex = getArguments().getInt(ARG_PARAM1);
            Log.d(DEBURG_TAG,"Arg = "+tabIndex);

            if(tabIndex == 0){
                recordState = EnrollmentActivity2.STATE_COMPLETE;
                cursor = rDbHelper.getHistoryTrxns(recordState);
            }else if(tabIndex == 1){
                recordState = EnrollmentActivity2.STATE_VERIFICATION;
                cursor = rDbHelper.getHistoryTrxns(recordState);
            }else if(tabIndex == 2){
                recordState = EnrollmentActivity2.STATE_ENROLLMENT;
                cursor = rDbHelper.getHistoryTrxns(recordState);
            }else{
                cursor = rDbHelper.getDocuments();
            }

            int numberOfRows = cursor.getCount();
            Log.d(DEBURG_TAG,"Count = "+numberOfRows);

            if(tabIndex == 0 || tabIndex == 1 || tabIndex == 2){
                while(numberOfRows>0 && cursor.moveToNext()){
                    ListItem item = new ListItem();
                    String name = cursor.getString(cursor.getColumnIndexOrThrow(ReaderContract.Employee.EMPLOYEE_NAME));
                    String id = cursor.getString(cursor.getColumnIndexOrThrow(ReaderContract.Employee.EMPLOYEE_ID));
                    Log.d(DEBURG_TAG,"EmployeeID = "+id);
                    String employer = cursor.getString(cursor.getColumnIndexOrThrow(ReaderContract.Employer.EMPLOYER_NAME));
                    String picPath = cursor.getString(cursor.getColumnIndexOrThrow(ReaderContract.Pics.NAME));
                    String ts = cursor.getString(cursor.getColumnIndexOrThrow(ReaderContract.State.TIME_STAMP));

                    String state;
                    if(tabIndex == 0){
                        state = "1";
                    }else if(tabIndex == 1){
                        state = "2";
                    }else{
                        state = "0";
                    }

                    item.setId(id);
                    item.setEmployee(name);
                    item.setEmployer(employer);
                    item.setDate(ts);
                    item.setThumbnailUrl(picPath);
                    item.setStatus(state);
                    item.setTabIndex(tabIndex);

                    //historyHash.put(employee, status);
                    historyList.add(item);
                }
            }else{
                while(numberOfRows>0 && cursor.moveToNext()){
                    ListItem item = new ListItem();
                    String picPath = cursor.getString(cursor.getColumnIndexOrThrow(ReaderContract.Pics.NAME));
                    String employee = cursor.getString(cursor.getColumnIndexOrThrow(ReaderContract.Employee.EMPLOYEE_NAME));
                    String status = cursor.getString(cursor.getColumnIndexOrThrow(ReaderContract.Pics.SYNCED));
                    String ts = cursor.getString(cursor.getColumnIndexOrThrow(ReaderContract.Pics.TS));

                    item.setId(picPath);
                    item.setEmployee(getPiname(picPath));
                    item.setEmployer(employee);
                    item.setDate(ts);
                    item.setStatus(status);
                    item.setThumbnailUrl(picPath);
                    item.setTabIndex(tabIndex);

                    //historyHash.put(employee, status);
                    historyList.add(item);
                }
            }
            cursor.close();
        }

        // TODO: Change Adapter to display your content
        adapter = new CustomListAdapter(getActivity(), historyList);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_histroysectionfragment, container, false);

        // Set the adapter
        mListView = (AbsListView) view.findViewById(android.R.id.list);
        ((AdapterView<ListAdapter>) mListView).setAdapter(adapter);

        // Set OnItemClickListener so we can be notified on item clicks
        mListView.setOnItemClickListener(this);

        return view;
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try {
            mListener = (OnFragmentInteractionListener) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString()
                + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }


    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        if (null != mListener) {
            String itemTS = historyList.get(position).getDate();
            int tabIndex = historyList.get(position).getTabIndex();
            // Notify the active callbacks interface (the activity, if the
            // fragment is attached to one) that an item has been selected.
            Log.d(DEBURG_TAG,"Tab Index = "+tabIndex);
            if(tabIndex<3){
                mListener.onFragmentInteraction(itemTS,recordState,position);
            }
        }
    }

    /**
     * The default content for this Fragment has a TextView that is shown when
     * the list is empty. If you would like to change the text, call this method
     * to supply the text it should use.
     */
    public void setEmptyText(CharSequence emptyText) {
        View emptyView = mListView.getEmptyView();

        if (emptyText instanceof TextView) {
            ((TextView) emptyView).setText(emptyText);
        }
    }

    /**
    * This interface must be implemented by activities that contain this
    * fragment to allow an interaction in this fragment to be communicated
    * to the activity and potentially other fragments contained in that
    * activity.
    * <p>
    * See the Android Training lesson <a href=
    * "http://developer.android.com/training/basics/fragments/communicating.html"
    * >Communicating with Other Fragments</a> for more information.
    */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        public void onFragmentInteraction(String ts,String state,int itemPosition);
    }

    private String getPiname(String path){
        String name = "";
        String [] array = path.split("/");
        if(array.length>0){
            name = array[array.length-1];
        }
        return name;
    }

}
