package com.peakbw.loanapp.fragments;

import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.os.Bundle;
import android.widget.DatePicker;
import android.widget.Toast;

import com.peakbw.loanapp.activities.EnrollmentActivity1;

public class DatePickerFragment extends DialogFragment implements DatePickerDialog.OnDateSetListener {
    private int year;
    private int viewID;

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        // Use the current date as the default date in the picker
        final Calendar c = Calendar.getInstance();
        year = c.get(Calendar.YEAR);
        int month = c.get(Calendar.MONTH);
        int day = c.get(Calendar.DAY_OF_MONTH);

        viewID = getArguments().getInt("view");

        // Create a new instance of DatePickerDialog and return it
        return new DatePickerDialog(getActivity(), this, year, month, day);
    }

    public void onDateSet(DatePicker view, int year1, int month, int day) {
        String mnth = String.valueOf(month+1);
        String dy = String.valueOf(day);

        if(mnth.length()==1){
            mnth = "0"+mnth;
        }

        if(dy.length()==1){
            dy = "0"+dy;
        }

        // get the difference between today and date of appointment
        Calendar today = Calendar.getInstance();
        today.setTime(new Date());

        Calendar dateOfAppointment = Calendar.getInstance();
        dateOfAppointment.set(year1,month,day);

        today.add(Calendar.DAY_OF_YEAR,-dateOfAppointment.get(Calendar.DAY_OF_YEAR));

        int difference = today.get(Calendar.YEAR) - dateOfAppointment.get(Calendar.YEAR);

        System.out.println("Diff = "+difference);


        if(viewID == EnrollmentActivity1.appointmentView){
            // check of difference is greater than 1 yaer
            if(difference>=1){
                EnrollmentActivity1.appointmentField.setText(year1+"-"+mnth+"-"+dy);
            }else{// trow error message
                EnrollmentActivity1.appointmentField.setError("Borrower not Eligible!");
                EnrollmentActivity1.appointmentField.requestFocus();
            }
        }
        else{
            // check if borrower is 18 years and above
            if(year-year1>17){
                EnrollmentActivity1.dateField.setText(year1+"-"+mnth+"-"+dy);
            }
            else{// trow error message
                EnrollmentActivity1.dateField.setError("Borrower should be 18yrs and above!");
                EnrollmentActivity1.dateField.requestFocus();
                //Toast.makeText(getActivity(), "invalid Date Of Birth!", Toast.LENGTH_SHORT).show();
            }
        }
    }
}
